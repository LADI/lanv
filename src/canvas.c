/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*
 * SPDX-FileCopyrightText: Copyright (C) 2007-2016 Dave Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2008-2025 Nedko Arnaudov
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "canvas.h"

struct canvas
{
#if 0
    Items         _items;       ///< Items on this canvas
    Edges         _edges;       ///< Edges ordered (src, dst)
    DstEdges      _dst_edges;   ///< Edges ordered (dst, src)
    Items         _selected_items; ///< Currently selected items
    SelectedEdges _selected_edges; ///< Currently selected edges

    SelectedPorts _selected_ports; ///< Selected ports (hilited red)
    LanvPort*     _connect_port; ///< Port for which a edge is being made
    LanvPort*     _last_selected_port;
    LanvEdge*     _drag_edge;
    LanvNode*     _drag_node;

    LanvBox* _select_rect;     ///< Rectangle for drag selection
    double   _select_start_x;  ///< Selection drag start x coordinate
    double   _select_start_y;  ///< Selection drag start y coordinate

    enum DragState { NOT_DRAGGING, EDGE, SCROLL, SELECT };
    DragState      _drag_state;

    GdkCursor* _move_cursor;
    guint      _animate_idle_id;

    PortOrderCtx _port_order;

    /* Root canvas item */
    LanvItem* root;

    /* Flow direction */
    LanvDirection direction;

    /* Canvas width */
    double width;

    /* Canvas height */
    double height;

    /* Region that needs redrawing (list of rectangles) */
    GSList* redraw_region;

    /* The item containing the mouse pointer, or NULL if none */
    LanvItem* current_item;

    /* Item that is about to become current (used to track deletions and such) */
    LanvItem* new_current_item;

    /* Item that holds a pointer grab, or NULL if none */
    LanvItem* grabbed_item;

    /* If non-NULL, the currently focused item */
    LanvItem* focused_item;

    /* GC for temporary draw pixmap */
    GdkGC* pixmap_gc;

    /* Event on which selection of current item is based */
    GdkEvent pick_event;

    /* Scrolling region */
    double scroll_x1;
    double scroll_y1;
    double scroll_x2;
    double scroll_y2;

    /* Scaling factor to be used for display */
    double pixels_per_unit;

    /* Font size in points */
    double font_size;

    /* Idle handler ID */
    guint idle_id;

    /* Signal handler ID for destruction of the root item */
    guint root_destroy_id;

    /* Area that is being redrawn.  Contains (x1, y1) but not (x2, y2).
     * Specified in canvas pixel coordinates.
     */
    int redraw_x1;
    int redraw_y1;
    int redraw_x2;
    int redraw_y2;

    /* Offsets of the temporary drawing pixmap */
    int draw_xofs;
    int draw_yofs;

    /* Internal pixel offsets when zoomed out */
    int zoom_xofs;
    int zoom_yofs;

    /* Last known modifier state, for deferred repick when a button is down */
    int state;

    /* Event mask specified when grabbing an item */
    guint grabbed_event_mask;

    /* Whether the canvas should center the scroll region in the middle of
     * the window if the scroll region is smaller than the window.
     */
    gboolean center_scroll_region;

    /* Whether items need update at next idle loop iteration */
    gboolean need_update;

    /* Whether the canvas needs redrawing at the next idle loop iteration */
    gboolean need_redraw;

    /* Whether current item will be repicked at next idle loop iteration */
    gboolean need_repick;

    /* For use by internal pick_current_item() function */
    gboolean left_grabbed_item;

    /* For use by internal pick_current_item() function */
    gboolean in_repick;

    /* Disable changes to canvas */
    gboolean locked;

    /* True if the current draw is an export */
    gboolean exporting;

#ifdef LANV_FDGL
    guint    layout_idle_id;
    gdouble  layout_energy;
    gboolean sprung_layout;
#endif
#endif
};

bool
canvas_init(
  void)
{
  return true;
}
 
bool
canvas_create(
  double width,
  double height,
  void * canvas_context,
  void (* connect_request)(void * port1_context, void * port2_context),
  void (* disconnect_request)(void * port1_context, void * port2_context),
  void (* module_location_changed)(void * module_context, double x, double y),
  void (* fill_canvas_menu)(void * menu, void * canvas_context),
  void (* fill_module_menu)(void * menu, void * module_context),
  void (* fill_port_menu)(void * menu, void * port_context),
  canvas_handle * canvas_handle_ptr)
{
#if 0
  boost::shared_ptr<canvas_cls> * canvas;

  canvas = new boost::shared_ptr<canvas_cls>(new canvas_cls(width,
                                                            height,
                                                            canvas_context,
                                                            connect_request,
                                                            disconnect_request,
                                                            module_location_changed,
                                                            fill_canvas_menu,
                                                            fill_module_menu,
                                                            fill_port_menu));

  *canvas_handle_ptr = (canvas_handle)canvas;
#endif
  return true;
}

//#define canvas_ptr ((boost::shared_ptr<canvas_cls> *)canvas)

void *
canvas_get_widget(
  canvas_handle canvas)
{
    return NULL;
//  return ((Gtk::Widget *)canvas_ptr->get())->gobj();
}

void
canvas_destroy(
  canvas_handle canvas)
{
//  delete canvas_ptr;
}

void
canvas_clear(
  canvas_handle canvas)
{
#if 0
  FlowCanvas::ItemList modules = canvas_ptr->get()->items(); // copy
  for (FlowCanvas::ItemList::iterator m = modules.begin(); m != modules.end(); ++m)
  {
    boost::shared_ptr<FlowCanvas::Module> module = boost::dynamic_pointer_cast<FlowCanvas::Module>(*m);
    if (!module)
      continue;

    FlowCanvas::PortVector ports = module->ports(); // copy
    for (FlowCanvas::PortVector::iterator p = ports.begin(); p != ports.end(); ++p)
    {
      boost::shared_ptr<FlowCanvas::Port> port = boost::dynamic_pointer_cast<FlowCanvas::Port>(*p);
      ASSERT(port != NULL);
      module->remove_port(port);
      port->hide();
    }

    ASSERT(module->ports().empty());
    canvas_ptr->get()->remove_item(module);
  }
#endif
}

void
canvas_get_size(
  canvas_handle canvas,
  double * width_ptr,
  double * height_ptr)
{
  *width_ptr = 1000;//canvas_ptr->get()->width();
  *height_ptr = 1000;//canvas_ptr->get()->height();
}

void
canvas_scroll_to_center(
  canvas_handle canvas)
{
//  if (canvas_ptr->get()->is_realized())
  {
    //log_info("realized");
//    canvas_ptr->get()->scroll_to_center();
  }
//  else
  {
    //log_info("NOT realized");
  }
}

#if 0
double
canvas_get_zoom(
  canvas_handle canvas)
{
//  return canvas_ptr->get()->get_zoom();
}

void
canvas_set_zoom(
  canvas_handle canvas,
  double pix_per_unit)
{
//  canvas_ptr->get()->set_zoom(pix_per_unit);
}
#endif

void
canvas_set_zoom_fit(
  canvas_handle canvas)
{
//  canvas_ptr->get()->zoom_full();
}

void
canvas_arrange(
  canvas_handle canvas)
{
#if 0
  Glib::RefPtr<Gdk::Window> win = canvas_ptr->get()->get_window();
  if (win)
  {
    canvas_ptr->get()->arrange();
  }
#endif
}

size_t
canvas_get_selected_modules_count(
  canvas_handle canvas)
{
  return 0;//canvas_ptr->get()->selected_items().size();
}

bool
canvas_get_one_selected_module(
  canvas_handle canvas,
  void ** module_context_ptr)
{
    return false;
#if 0
  int i;

  std::list<boost::shared_ptr<FlowCanvas::Item> > modules = canvas_ptr->get()->selected_items();
  if (modules.size() != 1)
  {
    return false;
  }

  i = 0;
	for (std::list<boost::shared_ptr<FlowCanvas::Item> >::iterator m = modules.begin(); m != modules.end(); ++m)
  {
    boost::shared_ptr<module_cls> module = boost::dynamic_pointer_cast<module_cls>(*m);
    if (module == NULL)
    {
      ASSERT_NO_PASS;
      return false;
    }

    if (i == 0)
    {
      *module_context_ptr = module->m_context;
      i++;
      continue;
    }
  }

  if (i != 1)
  {
    ASSERT_NO_PASS;
    return false;
  }

  return true;
#endif
}

bool
canvas_get_two_selected_modules(
  canvas_handle canvas,
  void ** module1_context_ptr,
  void ** module2_context_ptr)
{
    return false;
#if 0
  int i;

  std::list<boost::shared_ptr<FlowCanvas::Item> > modules = canvas_ptr->get()->selected_items();
  if (modules.size() != 2)
  {
    return false;
  }

  i = 0;
	for (std::list<boost::shared_ptr<FlowCanvas::Item> >::iterator m = modules.begin(); m != modules.end(); ++m)
  {
    boost::shared_ptr<module_cls> module = boost::dynamic_pointer_cast<module_cls>(*m);
    if (module == NULL)
    {
      ASSERT_NO_PASS;
      return false;
    }

    switch (i)
    {
    case 0:
      *module1_context_ptr = module->m_context;
      i++;
      continue;
    case 1:
      *module2_context_ptr = module->m_context;
      i++;
      continue;
    }
  }

  if (i != 2)
  {
    ASSERT_NO_PASS;
    return false;
  }

  return true;
#endif
}

bool
canvas_create_module(
  canvas_handle canvas,
  const char * name,
  double x,
  double y,
  bool show_title,
  bool show_port_labels,
  void * module_context,
  canvas_module_handle * module_handle_ptr)
{
//  boost::shared_ptr<FlowCanvas::Module> * module;

//  module = new boost::shared_ptr<FlowCanvas::Module>(new module_cls(*canvas_ptr, name, x, y, show_title, show_port_labels, module_context));
//  canvas_ptr->get()->add_item(*module);
//  module->get()->resize();

    *module_handle_ptr = NULL;//(canvas_module_handle)module;

  return true;
}

#define module_ptr ((client_t *)module)

void
canvas_set_module_name(
  canvas_module_handle module,
  const char * name)
{
//  module_ptr->get()->set_name(name);
//  module_ptr->get()->resize();
}

const char *
canvas_get_module_name(
  canvas_module_handle module)
{
//  return module_ptr->get()->name().c_str();
    return "TODO";
}

bool
canvas_destroy_module(
  canvas_handle canvas,
  canvas_module_handle module)
{
//  canvas_ptr->get()->remove_item(*module_ptr);
//  delete module_ptr;
    //_client_remove(&app, module_ptr);
    //_client_free(&app, module_ptr);
    return true;
}

bool
canvas_create_port(
  canvas_handle canvas,
  canvas_module_handle module,
  const char * name,
  bool is_input,
  bool is_midi,
  int color,
  void * port_context,
  canvas_port_handle * port_handle_ptr)
{
//  boost::shared_ptr<port_cls> * port;
//
//  port = new boost::shared_ptr<port_cls>(new port_cls(*module_ptr, name, is_input, color, port_context));
//
//  module_ptr->get()->add_port(*port);
//  module_ptr->get()->resize();

//  *port_handle_ptr = (canvas_port_handle)port;
//  *port_handle_ptr = NULL;

  return true;
}

#undef module_ptr
//#define port_ptr ((port_t *)port)

bool
canvas_destroy_port(
  canvas_handle canvas,
  canvas_port_handle port)
{
//  boost::shared_ptr<FlowCanvas::Module> module = port_ptr->get()->module().lock();
//  module->remove_port(*port_ptr);
//  delete port_ptr;
//  module->resize();
  return true;
}

#if 0
int
canvas_get_port_color(
  canvas_port_handle port)
{
//  return port_ptr->get()->color();
}
#endif

void
canvas_set_port_name(
  canvas_port_handle port,
  const char * name)
{
}

#if 0
const char *
canvas_get_port_name(
  canvas_port_handle port)
{
    return "TODO";//port_ptr->get()->name().c_str();
}
#endif

//#undef port_ptr
//#define port1_ptr ((port_t *)port1)
//#define port2_ptr ((port_t *)port2)

bool
canvas_add_connection(
  canvas_handle canvas,
  canvas_port_handle port1,
  canvas_port_handle port2,
  uint32_t color)
{
//  canvas_ptr->get()->add_connection(*port1_ptr, *port2_ptr, color);
//    port_conn_t *
  return true;
}

bool
canvas_remove_connection(
  canvas_handle canvas,
  canvas_port_handle port1,
  canvas_port_handle port2)
{
//  canvas_ptr->get()->remove_connection(*port1_ptr, *port2_ptr);
  return true;
}

//#undef port1_ptr
//#undef port2_ptr

bool
canvas_enum_modules(
  canvas_handle canvas,
  void * callback_context,
  bool (* callback)(void * context, canvas_module_handle module))
{
  return false;
}

bool
canvas_enum_module_ports(
  canvas_handle canvas,
  canvas_module_handle module,
  void * callback_context,
  bool (* callback)(void * context, canvas_port_handle port))
{
  return false;
}
