/* This file is part of Lanv.
 * Copyright 2007-2015 David Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2025 Nedko Arnaudov
 *
 * Lanv is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Lanv is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Lanv.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "boilerplate.h"
#include "color.h"
#include "lanv-private.h"
#include "gettext.h"

#include <lanv/box.h>
#include <lanv/canvas.h>
#include <lanv/edge.h>
#include <lanv/item.h>
#include <lanv/module.h>
#include <lanv/node.h>
#include <lanv/port.h>
#include <lanv/text.h>
#include <lanv/types.h>

#include <cairo.h>

#if 0

#include <gdk/gdk.h>
#include <glib-object.h>
#include <glib.h>
#include <gtk/gtk.h>

#include <math.h>
#include <stdarg.h>
#include <stdlib.h>

static const double PORT_LABEL_HPAD = 4.0;
static const double PORT_LABEL_VPAD = 1.0;

static void
lanv_port_update_control_slider(LanvPort* port, float value, gboolean force);

G_DEFINE_TYPE_WITH_CODE(LanvPort, lanv_port, LANV_TYPE_BOX,
                        G_ADD_PRIVATE(LanvPort))

static LanvBoxClass* parent_class;

enum {
	PROP_0,
	PROP_IS_INPUT,
	PROP_IS_CONTROLLABLE
};

enum {
	PORT_VALUE_CHANGED,
	PORT_LAST_SIGNAL
};

static guint port_signals[PORT_LAST_SIGNAL];

static void
lanv_port_init(LanvPort* port)
{
	port->impl = (LanvPortPrivate*)lanv_port_get_instance_private(port);

	port->impl->control         = NULL;
	port->impl->value_label     = NULL;
	port->impl->is_input        = TRUE;
	port->impl->is_controllable = FALSE;
}

static void
lanv_port_destroy(GtkObject* object)
{
	g_return_if_fail(object != NULL);
	g_return_if_fail(LANV_IS_PORT(object));

	LanvItem*   item   = LANV_ITEM(object);
	LanvPort*   port   = LANV_PORT(object);
	LanvCanvas* canvas = lanv_item_get_canvas(item);
	if (canvas) {
		if (port->impl->is_input) {
			lanv_canvas_for_each_edge_to(
				canvas, &port->box.node, (LanvEdgeFunc)lanv_edge_remove, NULL);
		} else {
			lanv_canvas_for_each_edge_from(
				canvas, &port->box.node, (LanvEdgeFunc)lanv_edge_remove, NULL);
		}
	}

	if (GTK_OBJECT_CLASS(parent_class)->destroy) {
		(*GTK_OBJECT_CLASS(parent_class)->destroy)(object);
	}
}

static void
lanv_port_set_property(GObject*      object,
                       guint         prop_id,
                       const GValue* value,
                       GParamSpec*   pspec)
{
	g_return_if_fail(object != NULL);
	g_return_if_fail(LANV_IS_PORT(object));

	LanvPort* port = LANV_PORT(object);

	switch (prop_id) {
		SET_CASE(IS_INPUT, boolean, port->impl->is_input)
		SET_CASE(IS_CONTROLLABLE, boolean, port->impl->is_controllable)
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

static void
lanv_port_get_property(GObject*    object,
                       guint       prop_id,
                       GValue*     value,
                       GParamSpec* pspec)
{
	g_return_if_fail(object != NULL);
	g_return_if_fail(LANV_IS_PORT(object));

	LanvPort* port = LANV_PORT(object);

	switch (prop_id) {
		GET_CASE(IS_INPUT, boolean, port->impl->is_input)
		GET_CASE(IS_CONTROLLABLE, boolean, port->impl->is_controllable)
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

static void
lanv_port_update(LanvItem* item, int flags)
{
	LanvPort*        port = LANV_PORT(item);
	LanvPortPrivate* impl = port->impl;

	if (impl->control) {
		lanv_item_invoke_update(LANV_ITEM(impl->control->rect), flags);
	}

	if (impl->value_label) {
		lanv_item_invoke_update(LANV_ITEM(port->impl->value_label), flags);
	}

	LanvItemClass* item_class = LANV_ITEM_CLASS(parent_class);
	item_class->update(item, flags);
}

static void
lanv_port_draw(LanvItem* item,
               cairo_t* cr, double cx, double cy, double cw, double ch)
{
	LanvPort*   port   = LANV_PORT(item);
	LanvCanvas* canvas = lanv_item_get_canvas(item);

	// Draw Box
	LanvItemClass* item_class = LANV_ITEM_CLASS(parent_class);
	item_class->draw(item, cr, cx, cy, cw, ch);

	if (port->impl->control) {
		// Clip to port boundaries (to stay within radiused borders)
		cairo_save(cr);
		const double  pad    = LANV_NODE(port)->impl->border_width / 2.0;
		LanvBoxCoords coords = LANV_BOX(port)->impl->coords;
		lanv_item_i2w_pair(LANV_ITEM(port),
		                   &coords.x1, &coords.y1, &coords.x2, &coords.y2);
		lanv_box_path(LANV_BOX(port), cr,
		              coords.x1 + pad, coords.y1 + pad,
		              coords.x2 - pad, coords.y2 - pad,
		              -pad);
		cairo_clip(cr);

		LanvItem* const rect = LANV_ITEM(port->impl->control->rect);
		LANV_ITEM_GET_CLASS(rect)->draw(rect, cr, cx, cy, cw, ch);

		cairo_restore(cr);
	}

	if (lanv_canvas_get_direction(canvas) == LANV_DIRECTION_DOWN ||
	    !LANV_NODE(port)->impl->show_label) {
		return;
	}

	LanvItem* labels[2] = {
		LANV_ITEM(LANV_NODE(item)->impl->label),
		port->impl->value_label ? LANV_ITEM(port->impl->value_label) : NULL
	};
	for (int i = 0; i < 2; ++i) {
		if (labels[i] && (labels[i]->object.flags & LANV_ITEM_VISIBLE)) {
			LANV_ITEM_GET_CLASS(labels[i])->draw(
				labels[i], cr, cx, cy, cw, ch);
		}
	}
}

static void
lanv_port_tail_vector(const LanvNode* self,
                      const LanvNode* head,
                      double*         x,
                      double*         y,
                      double*         dx,
                      double*         dy)
{
	(void)head;

	LanvPort*   port   = LANV_PORT(self);
	LanvItem*   item   = &port->box.node.item;
	LanvCanvas* canvas = lanv_item_get_canvas(item);

	const double px           = item->impl->x;
	const double py           = item->impl->y;
	const double border_width = LANV_NODE(port)->impl->border_width;

	switch (lanv_canvas_get_direction(canvas)) {
	case LANV_DIRECTION_RIGHT:
		*x  = px + lanv_box_get_width(&port->box) + (border_width / 2.0);
		*y  = py + lanv_box_get_height(&port->box) / 2.0;
		*dx = 1.0;
		*dy = 0.0;
		break;
	case LANV_DIRECTION_DOWN:
		*x  = px + lanv_box_get_width(&port->box) / 2.0;
		*y  = py + lanv_box_get_height(&port->box) + (border_width / 2.0);
		*dx = 0.0;
		*dy = 1.0;
		break;
	}

	lanv_item_i2w(item->impl->parent, x, y);
}

static void
lanv_port_head_vector(const LanvNode* self,
                      const LanvNode* tail,
                      double*         x,
                      double*         y,
                      double*         dx,
                      double*         dy)
{
	(void)tail;

	LanvPort*   port   = LANV_PORT(self);
	LanvItem*   item   = &port->box.node.item;
	LanvCanvas* canvas = lanv_item_get_canvas(item);

	const double px           = item->impl->x;
	const double py           = item->impl->y;
	const double border_width = LANV_NODE(port)->impl->border_width;

	switch (lanv_canvas_get_direction(canvas)) {
	case LANV_DIRECTION_RIGHT:
		*x  = px - (border_width / 2.0);
		*y  = py + lanv_box_get_height(&port->box) / 2.0;
		*dx = -1.0;
		*dy = 0.0;
		break;
	case LANV_DIRECTION_DOWN:
		*x  = px + lanv_box_get_width(&port->box) / 2.0;
		*y  = py - (border_width / 2.0);
		*dx = 0.0;
		*dy = -1.0;
		break;
	}

	lanv_item_i2w(item->impl->parent, x, y);
}

static void
lanv_port_place_labels(LanvPort* port)
{
	LanvCanvas*      canvas   = lanv_item_get_canvas(LANV_ITEM(port));
	LanvPortPrivate* impl     = port->impl;
	LanvText*        label    = LANV_NODE(port)->impl->label;
	const double     port_w   = lanv_box_get_width(&port->box);
	const double     port_h   = lanv_box_get_height(&port->box);
	double           vlabel_w = 0.0;
	if (impl->value_label) {
		const double vlabel_h = impl->value_label->impl->coords.height;
		vlabel_w = impl->value_label->impl->coords.width;
		if (lanv_canvas_get_direction(canvas) == LANV_DIRECTION_RIGHT) {
			lanv_item_set(LANV_ITEM(impl->value_label),
			              "x", PORT_LABEL_HPAD,
			              "y", (port_h - vlabel_h) / 2.0 - PORT_LABEL_VPAD,
			              NULL);
		} else {
			lanv_item_set(LANV_ITEM(impl->value_label),
			              "x", (port_w - vlabel_w) / 2.0,
			              "y", (port_h - vlabel_h) / 2.0 - PORT_LABEL_VPAD,
			              NULL);
		}
		vlabel_w += PORT_LABEL_HPAD;
	}
	if (label) {
		const double label_h = label->impl->coords.height;
		if (lanv_canvas_get_direction(canvas) == LANV_DIRECTION_RIGHT) {
			lanv_item_set(LANV_ITEM(label),
			              "x", vlabel_w + PORT_LABEL_HPAD,
			              "y", (port_h - label_h) / 2.0 - PORT_LABEL_VPAD,
			              NULL);
		}
	}
}

static void
lanv_port_resize(LanvNode* self)
{
	LanvPort* port   = LANV_PORT(self);
	LanvNode* node   = LANV_NODE(self);
	LanvText* label  = node->impl->label;
	LanvText* vlabel = port->impl->value_label;

	double label_w  = 0.0;
	double label_h  = 0.0;
	double vlabel_w = 0.0;
	double vlabel_h = 0.0;
	if (label && (LANV_ITEM(label)->object.flags & LANV_ITEM_VISIBLE)) {
		g_object_get(label, "width", &label_w, "height", &label_h, NULL);
	}
	if (vlabel && (LANV_ITEM(vlabel)->object.flags & LANV_ITEM_VISIBLE)) {
		g_object_get(vlabel, "width", &vlabel_w, "height", &vlabel_h, NULL);
	}

	if (label || vlabel) {
		double labels_w = label_w + PORT_LABEL_HPAD * 2.0;
		if (vlabel_w != 0.0) {
			labels_w += vlabel_w + PORT_LABEL_HPAD;
		}
		lanv_box_set_width(&port->box, labels_w);
		lanv_box_set_height(&port->box,
		                    MAX(label_h, vlabel_h) + (PORT_LABEL_VPAD * 2.0));

		lanv_port_place_labels(port);
	}

	if (LANV_NODE_CLASS(parent_class)->resize) {
		LANV_NODE_CLASS(parent_class)->resize(self);
	}
}

static void
lanv_port_redraw_text(LanvNode* node)
{
	LanvPort* port = LANV_PORT(node);
	if (port->impl->value_label) {
		lanv_text_layout(port->impl->value_label);
	}
	if (LANV_NODE_CLASS(parent_class)->redraw_text) {
		(*LANV_NODE_CLASS(parent_class)->redraw_text)(node);
	}
	lanv_port_place_labels(port);
}

static void
lanv_port_set_width(LanvBox* box,
                    double   width)
{
	LanvPort* port = LANV_PORT(box);
	parent_class->set_width(box, width);
	if (port->impl->control) {
		lanv_port_update_control_slider(port, port->impl->control->value, TRUE);
	}
	lanv_port_place_labels(port);
}

static void
lanv_port_set_height(LanvBox* box,
                     double   height)
{
	LanvPort* port = LANV_PORT(box);
	parent_class->set_height(box, height);
	if (port->impl->control) {
		lanv_item_set(LANV_ITEM(port->impl->control->rect),
		              "y1", box->impl->coords.border_width / 2.0,
		              "y2", height - box->impl->coords.border_width / 2.0,
		              NULL);
	}
	lanv_port_place_labels(port);
}

static gboolean
lanv_port_event(LanvItem* item, GdkEvent* event)
{
	LanvCanvas* canvas = lanv_item_get_canvas(item);

	return lanv_canvas_port_event(canvas, LANV_PORT(item), event);
}

static void
lanv_port_class_init(LanvPortClass* klass)
{
	GObjectClass*   gobject_class = (GObjectClass*)klass;
	GtkObjectClass* object_class  = (GtkObjectClass*)klass;
	LanvItemClass*  item_class    = (LanvItemClass*)klass;
	LanvNodeClass*  node_class    = (LanvNodeClass*)klass;
	LanvBoxClass*   box_class     = (LanvBoxClass*)klass;

	parent_class = LANV_BOX_CLASS(g_type_class_peek_parent(klass));

	gobject_class->set_property = lanv_port_set_property;
	gobject_class->get_property = lanv_port_get_property;

	g_object_class_install_property(
		gobject_class, PROP_IS_INPUT, g_param_spec_boolean(
			"is-input",
			_("Is input"),
			_("Whether this port is an input, rather than an output."),
			0,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_IS_CONTROLLABLE, g_param_spec_boolean(
			"is-controllable",
			_("Is controllable"),
			_("Whether this port can be controlled by the user."),
			0,
			G_PARAM_READWRITE));

	port_signals[PORT_VALUE_CHANGED]
	    = g_signal_new("value-changed",
	                   G_TYPE_FROM_CLASS(klass),
	                   G_SIGNAL_RUN_LAST,
	                   0,
	                   NULL, NULL,
	                   NULL,
	                   G_TYPE_NONE, 1,
	                   G_TYPE_DOUBLE);

	object_class->destroy = lanv_port_destroy;

	item_class->update = lanv_port_update;
	item_class->event  = lanv_port_event;
	item_class->draw   = lanv_port_draw;

	node_class->tail_vector = lanv_port_tail_vector;
	node_class->head_vector = lanv_port_head_vector;
	node_class->resize      = lanv_port_resize;
	node_class->redraw_text = lanv_port_redraw_text;

	box_class->set_width  = lanv_port_set_width;
	box_class->set_height = lanv_port_set_height;
}

LanvPort*
lanv_port_new(LanvModule* module,
              gboolean    is_input,
              const char* first_prop_name, ...)
{
	LanvPort* port = LANV_PORT(g_object_new(lanv_port_get_type(), NULL));

	port->impl->is_input = is_input;

	LanvItem* item = LANV_ITEM(port);
	va_list args;
	va_start(args, first_prop_name);
	lanv_item_construct(item,
	                    LANV_ITEM(module),
	                    first_prop_name, args);
	va_end(args);

	LanvBox* box = LANV_BOX(port);
	box->impl->coords.border_width = 1.0;

	LanvNode* node = LANV_NODE(port);
	node->impl->can_tail     = !is_input;
	node->impl->can_head     = is_input;
	node->impl->draggable    = FALSE;
	node->impl->border_width = 2.0;

	LanvCanvas* canvas = lanv_item_get_canvas(LANV_ITEM(port));
	lanv_port_set_direction(port, lanv_canvas_get_direction(canvas));

	return port;
}

void
lanv_port_set_direction(LanvPort*     port,
                        LanvDirection direction)
{
	LanvNode* node     = LANV_NODE(port);
	LanvBox*  box      = LANV_BOX(port);
	gboolean  is_input = port->impl->is_input;
	switch (direction) {
	case LANV_DIRECTION_RIGHT:
		box->impl->radius_tl = (is_input ? 0.0 : 5.0);
		box->impl->radius_tr = (is_input ? 5.0 : 0.0);
		box->impl->radius_br = (is_input ? 5.0 : 0.0);
		box->impl->radius_bl = (is_input ? 0.0 : 5.0);
		break;
	case LANV_DIRECTION_DOWN:
		box->impl->radius_tl = (is_input ? 0.0 : 5.0);
		box->impl->radius_tr = (is_input ? 0.0 : 5.0);
		box->impl->radius_br = (is_input ? 5.0 : 0.0);
		box->impl->radius_bl = (is_input ? 5.0 : 0.0);
		break;
	}

	node->impl->must_resize = TRUE;
	lanv_item_request_update(LANV_ITEM(node));
}

void
lanv_port_show_control(LanvPort* port)
{
	if (port->impl->control) {
		return;
	}

	const guint  color        = 0xFFFFFF66;
	const double border_width = LANV_NODE(port)->impl->border_width;

	LanvPortControl* control = (LanvPortControl*)malloc(sizeof(LanvPortControl));
	port->impl->control = control;

	control->value      = 0.0f;
	control->min        = 0.0f;
	control->max        = 1.0f;
	control->is_toggle  = FALSE;
	control->is_integer = FALSE;
	control->rect       = LANV_BOX(
		lanv_item_new(LANV_ITEM(port),
		              lanv_box_get_type(),
		              "x1", border_width / 2.0,
		              "y1", border_width / 2.0,
		              "x2", 0.0,
		              "y2", lanv_box_get_height(&port->box) - border_width / 2.0,
		              "fill-color", color,
		              "border-color", color,
		              "border-width", 0.0,
		              "managed", TRUE,
		              NULL));
	lanv_item_show(LANV_ITEM(control->rect));
}

void
lanv_port_hide_control(LanvPort* port)
{
	gtk_object_destroy(GTK_OBJECT(port->impl->control->rect));
	free(port->impl->control);
	port->impl->control = NULL;
}

void
lanv_port_set_value_label(LanvPort*   port,
                          const char* str)
{
	LanvPortPrivate* impl = port->impl;

	if (!str || str[0] == '\0') {
		if (impl->value_label) {
			gtk_object_destroy(GTK_OBJECT(impl->value_label));
			impl->value_label = NULL;
		}
	} else if (impl->value_label) {
		lanv_item_set(LANV_ITEM(impl->value_label),
		              "text", str,
		              NULL);
	} else {
		impl->value_label = LANV_TEXT(lanv_item_new(LANV_ITEM(port),
		                                            lanv_text_get_type(),
		                                            "text", str,
		                                            "color", DIM_TEXT_COLOR,
		                                            "managed", TRUE,
		                                            NULL));
	}
}

static void
lanv_port_update_control_slider(LanvPort* port, float value, gboolean force)
{
	LanvPortPrivate* impl = port->impl;
	if (!impl->control) {
		return;
	}

	// Clamp to toggle or integer value if applicable
	if (impl->control->is_toggle) {
		if (value != 0.0f) {
			value = impl->control->max;
		} else {
			value = impl->control->min;
		}
	} else if (impl->control->is_integer) {
		value = lrintf(value);
	}

	// Clamp to range
	if (value < impl->control->min) {
		value = impl->control->min;
	}
	if (value > impl->control->max) {
		value = impl->control->max;
	}

	if (!force && value == impl->control->value) {
		return;  // No change, do nothing
	}

	const double span = (lanv_box_get_width(&port->box) -
	                     LANV_NODE(port)->impl->border_width);

	const double w = (value - impl->control->min)
		/ (impl->control->max - impl->control->min)
		* span;

	if (isnan(w)) {
		return;  // Shouldn't happen, but ignore crazy values
	}

	// Redraw port
	impl->control->value = value;
	lanv_box_set_width(impl->control->rect, MAX(0.0, w));
	lanv_box_request_redraw(
		LANV_ITEM(port), &LANV_BOX(port)->impl->coords, FALSE);
}

void
lanv_port_set_control_is_toggle(LanvPort* port,
                                gboolean  is_toggle)
{
	if (port->impl->control) {
		port->impl->control->is_toggle = is_toggle;
		lanv_port_update_control_slider(port, port->impl->control->value, TRUE);
	}
}

void
lanv_port_set_control_is_integer(LanvPort* port,
                                 gboolean  is_integer)
{
	if (port->impl->control) {
		port->impl->control->is_integer = is_integer;
		const float rounded = rintf(port->impl->control->value);
		lanv_port_update_control_slider(port, rounded, TRUE);
	}
}

void
lanv_port_set_control_value(LanvPort* port,
                            float     value)
{
	lanv_port_update_control_slider(port, value, FALSE);
}

void
lanv_port_set_control_value_internal(LanvPort* port,
                                     float     value)
{
	// Update slider
	lanv_port_set_control_value(port, value);

	// Fire signal to notify user value has changed
	const double dvalue = port->impl->control->value;
	g_signal_emit(port, port_signals[PORT_VALUE_CHANGED], 0, dvalue, NULL);
}

void
lanv_port_set_control_min(LanvPort* port,
                          float     min)
{
	if (port->impl->control) {
		const gboolean force = port->impl->control->min != min;
		port->impl->control->min = min;
		if (port->impl->control->max < min) {
			port->impl->control->max = min;
		}
		lanv_port_update_control_slider(port, port->impl->control->value, force);
	}
}

void
lanv_port_set_control_max(LanvPort* port,
                          float     max)
{
	if (port->impl->control) {
		const gboolean force = port->impl->control->max != max;
		port->impl->control->max = max;
		if (port->impl->control->min > max) {
			port->impl->control->min = max;
		}
		lanv_port_update_control_slider(port, port->impl->control->value, force);
	}
}

double
lanv_port_get_natural_width(const LanvPort* port)
{
	LanvCanvas* const canvas = lanv_item_get_canvas(LANV_ITEM(port));
	LanvText* const   label  = port->box.node.impl->label;
	double            w      = 0.0;
	if (lanv_canvas_get_direction(canvas) == LANV_DIRECTION_DOWN) {
		w = lanv_module_get_empty_port_breadth(lanv_port_get_module(port));
	} else if (label && (LANV_ITEM(label)->object.flags & LANV_ITEM_VISIBLE)) {
		double label_w = 0.0;
		g_object_get(port->box.node.impl->label, "width", &label_w, NULL);
		w = label_w + (PORT_LABEL_HPAD * 2.0);
	} else {
		w = lanv_module_get_empty_port_depth(lanv_port_get_module(port));
	}
	if (port->impl->value_label &&
	    (LANV_ITEM(port->impl->value_label)->object.flags
	     & LANV_ITEM_VISIBLE)) {
		double label_w = 0.0;
		g_object_get(port->impl->value_label, "width", &label_w, NULL);
		w += label_w + PORT_LABEL_HPAD;
	}
	return w;
}

LanvModule*
lanv_port_get_module(const LanvPort* port)
{
	return LANV_MODULE(LANV_ITEM(port)->impl->parent);
}

float
lanv_port_get_control_value(const LanvPort* port)
{
	return port->impl->control ? port->impl->control->value : 0.0f;
}

float
lanv_port_get_control_min(const LanvPort* port)
{
	return port->impl->control ? port->impl->control->min : 0.0f;
}

float
lanv_port_get_control_max(const LanvPort* port)
{
	return port->impl->control ? port->impl->control->max : 0.0f;
}

gboolean
lanv_port_is_input(const LanvPort* port)
{
	return port->impl->is_input;
}

gboolean
lanv_port_is_output(const LanvPort* port)
{
	return !port->impl->is_input;
}

#endif
