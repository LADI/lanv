/* This file is part of Lanv.
 * Copyright 2007-2015 David Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2025 Nedko Arnaudov
 *
 * Lanv is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Lanv is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Lanv.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "color.h"

#include <lanv/Box.hpp>
#include <lanv/Module.hpp>
#include <lanv/Port.hpp>
#include <lanv/box.h>
#include <lanv/port.h>
#include <lanv/types.h>

#include <glib-object.h>
#include <gobject/gclosure.h>
#include <sigc++/signal.h>

#include <cstddef>
#include <cstdint>
#include <string>

namespace Lanv {

static void
on_value_changed(LanvPort* port, double value, void* portmm)
{
	(void)port;

	static_cast<Port*>(portmm)->signal_value_changed.emit(value);
}

/* Construct a Port on an existing module. */
Port::Port(Module&            module,
           const std::string& name,
           bool               is_input,
           uint32_t           color)
	: Box(module.canvas(),
	      LANV_BOX(lanv_port_new(module.gobj(), is_input,
	                             "fill-color", color,
	                             "border-color", PORT_BORDER_COLOR(color),
	                             "border-width", 2.0,
	                             "label", name.c_str(),
	                             NULL)))
{
	g_signal_connect(gobj(), "value-changed",
	                 G_CALLBACK(on_value_changed), this);
}

Module*
Port::get_module() const
{
	return Glib::wrap(lanv_port_get_module(gobj()));
}

} // namespace Lanv
