/* This file is part of Lanv.
 * Copyright 2007-2016 David Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2025 Nedko Arnaudov
 *
 * Lanv is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Lanv is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Lanv.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "boilerplate.h"
#include "color.h"
#if LANV_GIR_ENABLED
#include "lanv-marshal.h"
#endif
#include "lanv-private.h"
#include "gettext.h"

#include <lanv/canvas.h>
#include <lanv/edge.h>
#include <lanv/item.h>
#include <lanv/node.h>
#include <lanv/text.h>
#include <lanv/types.h>

#include <cairo.h>
#if 0
#include <gdk/gdk.h>
#include <glib-object.h>
#include <glib.h>
#include <gtk/gtk.h>

#include <math.h>
#include <stddef.h>

guint signal_moved;

G_DEFINE_TYPE_WITH_CODE(LanvNode, lanv_node, LANV_TYPE_ITEM,
                        G_ADD_PRIVATE(LanvNode))

static LanvItemClass* parent_class;

enum {
	PROP_0,
	PROP_CANVAS,
	PROP_PARTNER,
	PROP_LABEL,
	PROP_SHOW_LABEL,
	PROP_DASH_LENGTH,
	PROP_DASH_OFFSET,
	PROP_BORDER_WIDTH,
	PROP_FILL_COLOR,
	PROP_BORDER_COLOR,
	PROP_CAN_TAIL,
	PROP_CAN_HEAD,
	PROP_IS_SOURCE,
	PROP_SELECTED,
	PROP_HIGHLIGHTED,
	PROP_DRAGGABLE,
	PROP_GRABBED
};

static void
lanv_node_init(LanvNode* node)
{
	LanvNodePrivate* impl =
	    (LanvNodePrivate*)lanv_node_get_instance_private(node);

	node->impl = impl;

	impl->partner      = NULL;
	impl->label        = NULL;
	impl->dash_length  = 0.0;
	impl->dash_offset  = 0.0;
	impl->border_width = 2.0;
	impl->fill_color   = DEFAULT_FILL_COLOR;
	impl->border_color = DEFAULT_BORDER_COLOR;
	impl->can_tail     = FALSE;
	impl->can_head     = FALSE;
	impl->is_source    = FALSE;
	impl->selected     = FALSE;
	impl->highlighted  = FALSE;
	impl->draggable    = FALSE;
	impl->show_label   = TRUE;
	impl->grabbed      = FALSE;
	impl->must_resize  = FALSE;
#ifdef LANV_FDGL
	impl->force.x       = 0.0;
	impl->force.y       = 0.0;
	impl->vel.x         = 0.0;
	impl->vel.y         = 0.0;
	impl->connected     = FALSE;
#endif
}

static void
lanv_node_realize(LanvItem* item)
{
	LANV_ITEM_CLASS(parent_class)->realize(item);
	lanv_canvas_add_node(lanv_item_get_canvas(item), LANV_NODE(item));
}

static void
lanv_node_destroy(GtkObject* object)
{
	g_return_if_fail(object != NULL);
	g_return_if_fail(LANV_IS_NODE(object));

	LanvNode*        node = LANV_NODE(object);
	LanvNodePrivate* impl = node->impl;
	if (impl->label) {
		g_object_unref(impl->label);
		impl->label = NULL;
	}

	LanvItem* item = LANV_ITEM(object);
	lanv_node_disconnect(node);
	if (item->impl->canvas) {
		lanv_canvas_remove_node(item->impl->canvas, node);
	}

	if (GTK_OBJECT_CLASS(parent_class)->destroy) {
		(*GTK_OBJECT_CLASS(parent_class)->destroy)(object);
	}

	impl->partner      = NULL;
	item->impl->canvas = NULL;
}

/// Expands the canvas to contain a moved/resized node if necessary
static void
lanv_node_expand_canvas(LanvNode* node)
{
	LanvItem*   item   = LANV_ITEM(node);
	LanvCanvas* canvas = lanv_item_get_canvas(item);

	if (item->impl->parent == lanv_canvas_root(canvas)) {
		const double pad = 10.0;

		double x1 = 0.0;
		double y1 = 0.0;
		double x2 = 0.0;
		double y2 = 0.0;
		lanv_item_get_bounds(item, &x1, &y1, &x2, &y2);

		lanv_item_i2w(item, &x1, &y1);
		lanv_item_i2w(item, &x2, &y2);

		double canvas_w = 0.0;
		double canvas_h = 0.0;
		lanv_canvas_get_size(canvas, &canvas_w, &canvas_h);
		if (x2 + pad > canvas_w || y2 + pad > canvas_h) {
			lanv_canvas_resize(canvas,
			                   MAX(x1 + pad, canvas_w),
			                   MAX(y2 + pad, canvas_h));
		}
	}
}

static void
lanv_node_update(LanvItem* item, int flags)
{
	LanvNode* node = LANV_NODE(item);
	if (node->impl->must_resize) {
		lanv_node_resize(node);
		node->impl->must_resize = FALSE;
	}

	if (node->impl->label) {
		lanv_item_invoke_update(LANV_ITEM(node->impl->label), flags);
	}

	LANV_ITEM_CLASS(parent_class)->update(item, flags);

	lanv_node_expand_canvas(node);
}

static void
lanv_node_draw(LanvItem* item,
               cairo_t* cr, double cx, double cy, double cw, double ch)
{
	(void)item;
	(void)cr;
	(void)cx;
	(void)cy;
	(void)cw;
	(void)ch;

	/* TODO: Label is not drawn here because ports need to draw control
	   rects then the label on top.  I can't see a way of solving this since
	   there's no single time parent class draw needs to be called, so perhaps
	   label shouldn't be part of this class... */
}

static void
lanv_node_set_property(GObject*      object,
                       guint         prop_id,
                       const GValue* value,
                       GParamSpec*   pspec)
{
	g_return_if_fail(object != NULL);
	g_return_if_fail(LANV_IS_NODE(object));

	LanvNode*        node = LANV_NODE(object);
	LanvNodePrivate* impl = node->impl;

	switch (prop_id) {
		SET_CASE(DASH_LENGTH, double, impl->dash_length)
		SET_CASE(DASH_OFFSET, double, impl->dash_offset)
		SET_CASE(BORDER_WIDTH, double, impl->border_width)
		SET_CASE(FILL_COLOR, uint, impl->fill_color)
		SET_CASE(BORDER_COLOR, uint, impl->border_color)
		SET_CASE(CAN_TAIL, boolean, impl->can_tail)
		SET_CASE(CAN_HEAD, boolean, impl->can_head)
		SET_CASE(IS_SOURCE, boolean, impl->is_source)
		SET_CASE(HIGHLIGHTED, boolean, impl->highlighted)
		SET_CASE(DRAGGABLE, boolean, impl->draggable)
		SET_CASE(GRABBED, boolean, impl->grabbed)
	case PROP_PARTNER:
		impl->partner = (LanvNode*)g_value_get_object(value);
		break;
	case PROP_SELECTED:
		if (impl->selected != g_value_get_boolean(value)) {
			LanvItem* item = LANV_ITEM(object);
			impl->selected = g_value_get_boolean(value);
			if (item->impl->canvas) {
				if (impl->selected) {
					lanv_canvas_select_node(lanv_item_get_canvas(item), node);
				} else {
					lanv_canvas_unselect_node(lanv_item_get_canvas(item), node);
				}
				lanv_item_request_update(item);
			}
		}
		break;
	case PROP_CANVAS:
		if (!LANV_ITEM(object)->impl->parent) {
			LanvCanvas* canvas = LANV_CANVAS(g_value_get_object(value));
			g_object_set(object, "parent", lanv_canvas_root(canvas), NULL);
			lanv_canvas_add_node(canvas, node);
		} else {
			g_warning("Cannot change `canvas' property after construction");
		}
		break;
	case PROP_LABEL:
		lanv_node_set_label(node, g_value_get_string(value));
		break;
	case PROP_SHOW_LABEL:
		lanv_node_set_show_label(node, g_value_get_boolean(value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

static void
lanv_node_get_property(GObject*    object,
                       guint       prop_id,
                       GValue*     value,
                       GParamSpec* pspec)
{
	g_return_if_fail(object != NULL);
	g_return_if_fail(LANV_IS_NODE(object));

	LanvNode*        node = LANV_NODE(object);
	LanvNodePrivate* impl = node->impl;

	switch (prop_id) {
		GET_CASE(PARTNER, object, impl->partner)
		GET_CASE(LABEL, string, impl->label ? impl->label->impl->text : NULL)
		GET_CASE(DASH_LENGTH, double, impl->dash_length)
		GET_CASE(DASH_OFFSET, double, impl->dash_offset)
		GET_CASE(BORDER_WIDTH, double, impl->border_width)
		GET_CASE(FILL_COLOR, uint, impl->fill_color)
		GET_CASE(BORDER_COLOR, uint, impl->border_color)
		GET_CASE(CAN_TAIL, boolean, impl->can_tail)
		GET_CASE(CAN_HEAD, boolean, impl->can_head)
		GET_CASE(IS_SOURCE, boolean, impl->is_source)
		GET_CASE(SELECTED, boolean, impl->selected)
		GET_CASE(HIGHLIGHTED, boolean, impl->highlighted)
		GET_CASE(DRAGGABLE, boolean, impl->draggable)
		GET_CASE(GRABBED, boolean, impl->grabbed)
	case PROP_CANVAS:
		g_value_set_object(value, lanv_item_get_canvas(LANV_ITEM(object)));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

static void
lanv_node_default_tail_vector(const LanvNode* self,
                              const LanvNode* head,
                              double*         x,
                              double*         y,
                              double*         dx,
                              double*         dy)
{
	(void)head;

	LanvCanvas* canvas = lanv_item_get_canvas(LANV_ITEM(self));

	*x = LANV_ITEM(self)->impl->x;
	*y = LANV_ITEM(self)->impl->y;

	switch (lanv_canvas_get_direction(canvas)) {
	case LANV_DIRECTION_RIGHT:
		*dx = 1.0;
		*dy = 0.0;
		break;
	case LANV_DIRECTION_DOWN:
		*dx = 0.0;
		*dy = 1.0;
		break;
	}

	lanv_item_i2w(LANV_ITEM(self)->impl->parent, x, y);
}

static void
lanv_node_default_head_vector(const LanvNode* self,
                              const LanvNode* tail,
                              double*         x,
                              double*         y,
                              double*         dx,
                              double*         dy)
{
	(void)tail;

	LanvCanvas* canvas = lanv_item_get_canvas(LANV_ITEM(self));

	*x = LANV_ITEM(self)->impl->x;
	*y = LANV_ITEM(self)->impl->y;

	switch (lanv_canvas_get_direction(canvas)) {
	case LANV_DIRECTION_RIGHT:
		*dx = -1.0;
		*dy = 0.0;
		break;
	case LANV_DIRECTION_DOWN:
		*dx = 0.0;
		*dy = -1.0;
		break;
	}

	lanv_item_i2w(LANV_ITEM(self)->impl->parent, x, y);
}

void
lanv_node_get_draw_properties(const LanvNode* node,
                              double*         dash_length,
                              double*         border_color,
                              double*         fill_color)
{
	LanvNodePrivate* impl = node->impl;

	*dash_length  = impl->dash_length;
	*border_color = impl->border_color;
	*fill_color   = impl->fill_color;

	if (impl->selected) {
		*dash_length  = 4.0;
		*border_color = highlight_color(impl->border_color, 0x40);
	}

	if (impl->highlighted) {
		*border_color = highlight_color(impl->border_color, 0x40);
		*fill_color   = impl->fill_color;
	}
}

void
lanv_node_set_label(LanvNode* node, const char* str)
{
	LanvNodePrivate* impl = node->impl;
	if (!str || str[0] == '\0') {
		if (impl->label) {
			gtk_object_destroy(GTK_OBJECT(impl->label));
			impl->label = NULL;
		}
	} else if (impl->label) {
		lanv_item_set(LANV_ITEM(impl->label),
		              "text", str,
		              NULL);
	} else {
		impl->label = LANV_TEXT(lanv_item_new(LANV_ITEM(node),
		                                      lanv_text_get_type(),
		                                      "text", str,
		                                      "color", DEFAULT_TEXT_COLOR,
		                                      "managed", TRUE,
		                                      NULL));
	}

	impl->must_resize = TRUE;
	lanv_item_request_update(LANV_ITEM(node));
}

void
lanv_node_set_show_label(LanvNode* node, gboolean show)
{
	if (node->impl->label) {
		if (show) {
			lanv_item_show(LANV_ITEM(node->impl->label));
		} else {
			lanv_item_hide(LANV_ITEM(node->impl->label));
		}
	}
	node->impl->show_label = show;
	lanv_item_request_update(LANV_ITEM(node));
}

static void
lanv_node_default_tick(LanvNode* self,
                       double    seconds)
{
	LanvNode* node = LANV_NODE(self);
	node->impl->dash_offset = seconds * 8.0;
	lanv_item_request_update(LANV_ITEM(self));
}

static void
lanv_node_default_disconnect(LanvNode* node)
{
	LanvCanvas* canvas = lanv_item_get_canvas(LANV_ITEM(node));
	if (canvas) {
		lanv_canvas_for_each_edge_on(
			canvas, node, (LanvEdgeFunc)lanv_edge_disconnect, NULL);
	}
}

static void
lanv_node_default_move(LanvNode* node,
                       double    dx,
                       double    dy)
{
	LanvCanvas* canvas = lanv_item_get_canvas(LANV_ITEM(node));
	lanv_item_move(LANV_ITEM(node), dx, dy);
	lanv_canvas_for_each_edge_on(
		canvas, node, (LanvEdgeFunc)lanv_edge_update_location, NULL);
	lanv_item_request_update(LANV_ITEM(node));
}

static void
lanv_node_default_move_to(LanvNode* node,
                          double    x,
                          double    y)
{
	LanvItem*   item   = LANV_ITEM(node);
	LanvCanvas* canvas = lanv_item_get_canvas(item);
	item->impl->x = x;
	item->impl->y = y;
	if (node->impl->can_tail) {
		lanv_canvas_for_each_edge_from(
			canvas, node, (LanvEdgeFunc)lanv_edge_update_location, NULL);
	} else if (node->impl->can_head) {
		lanv_canvas_for_each_edge_to(
			canvas, node, (LanvEdgeFunc)lanv_edge_update_location, NULL);
	}
	lanv_item_request_update(LANV_ITEM(node));
}

static void
lanv_node_default_resize(LanvNode* node)
{
	LanvItem* item = LANV_ITEM(node);
	if (LANV_IS_NODE(item->impl->parent)) {
		lanv_node_resize(LANV_NODE(item->impl->parent));
	}
	node->impl->must_resize = FALSE;
}

static void
lanv_node_default_redraw_text(LanvNode* node)
{
	if (node->impl->label) {
		lanv_text_layout(node->impl->label);
		node->impl->must_resize = TRUE;
		lanv_item_request_update(LANV_ITEM(node));
	}
}

static gboolean
lanv_node_default_event(LanvItem* item,
                        GdkEvent* event)
{
	LanvNode*   node   = LANV_NODE(item);
	LanvCanvas* canvas = lanv_item_get_canvas(LANV_ITEM(node));

	// FIXME: put these somewhere better
	static double   last_x       = NAN;
	static double   last_y       = NAN;
	static double   drag_start_x = NAN;
	static double   drag_start_y = NAN;
	static gboolean dragging     = FALSE;

	switch (event->type) {
	case GDK_ENTER_NOTIFY:
		lanv_item_raise(LANV_ITEM(node));
		node->impl->highlighted = TRUE;
		lanv_item_request_update(item);
		return TRUE;

	case GDK_LEAVE_NOTIFY:
		lanv_item_lower(LANV_ITEM(node));
		node->impl->highlighted = FALSE;
		lanv_item_request_update(item);
		return TRUE;

	case GDK_BUTTON_PRESS:
		drag_start_x = event->button.x;
		drag_start_y = event->button.y;
		last_x       = event->button.x;
		last_y       = event->button.y;
		if (!lanv_canvas_get_locked(canvas) && node->impl->draggable && event->button.button == 1) {
			lanv_canvas_grab_item(
				LANV_ITEM(node),
				GDK_POINTER_MOTION_MASK|GDK_BUTTON_RELEASE_MASK|GDK_BUTTON_PRESS_MASK,
				lanv_canvas_get_move_cursor(canvas),
				event->button.time);
			node->impl->grabbed = TRUE;
			dragging = TRUE;
			return TRUE;
		}
		break;

	case GDK_BUTTON_RELEASE:
		if (dragging) {
			gboolean selected = FALSE;
			g_object_get(G_OBJECT(node), "selected", &selected, NULL);
			lanv_canvas_ungrab_item(LANV_ITEM(node), event->button.time);
			node->impl->grabbed = FALSE;
			dragging = FALSE;
			if (event->button.x != drag_start_x || event->button.y != drag_start_y) {
				lanv_canvas_contents_changed(canvas);
				if (selected) {
					lanv_canvas_selection_move_finished(canvas);
				} else {
					const double x = LANV_ITEM(node)->impl->x;
					const double y = LANV_ITEM(node)->impl->y;
					g_signal_emit(node, signal_moved, 0, x, y, NULL);
				}
			} else {
				// Clicked
				if (selected) {
					lanv_canvas_unselect_node(canvas, node);
				} else {
					if (!(event->button.state & (GDK_CONTROL_MASK | GDK_SHIFT_MASK))) {
						lanv_canvas_clear_selection(canvas);
					}
					lanv_canvas_select_node(canvas, node);
				}
			}
			return TRUE;
		}
		break;

	case GDK_MOTION_NOTIFY:
		if ((dragging && (event->motion.state & GDK_BUTTON1_MASK))) {
			gboolean selected = FALSE;
			g_object_get(G_OBJECT(node), "selected", &selected, NULL);

			double new_x = event->motion.x;
			double new_y = event->motion.y;

			if (event->motion.is_hint) {
				int             t_x   = 0;
				int             t_y   = 0;
				GdkModifierType state = (GdkModifierType)0;
				gdk_window_get_pointer(event->motion.window, &t_x, &t_y, &state);
				new_x = t_x;
				new_y = t_y;
			}

			const double dx = new_x - last_x;
			const double dy = new_y - last_y;
			if (selected) {
				lanv_canvas_move_selected_items(canvas, dx, dy);
			} else {
				lanv_node_move(node, dx, dy);
			}

			last_x = new_x;
			last_y = new_y;
			return TRUE;
		}

	default:
		break;
	}

	return FALSE;
}

static void
lanv_node_class_init(LanvNodeClass* klass)
{
	GObjectClass*   gobject_class = (GObjectClass*)klass;
	GtkObjectClass* object_class  = (GtkObjectClass*)klass;
	LanvItemClass*  item_class    = (LanvItemClass*)klass;

	parent_class = LANV_ITEM_CLASS(g_type_class_peek_parent(klass));

	gobject_class->set_property = lanv_node_set_property;
	gobject_class->get_property = lanv_node_get_property;

	g_object_class_install_property(
		gobject_class, PROP_CANVAS, g_param_spec_object(
			"canvas",
			_("Canvas"),
			_("The canvas this node is on."),
			LANV_TYPE_CANVAS,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_PARTNER, g_param_spec_object(
			"partner",
			_("Partner"),
			_("Partners are nodes that should be visually aligned to correspond"
			  " to each other, even if they are not necessarily connected (e.g."
			  " for separate modules representing the inputs and outputs of a"
			  " single thing).  When the canvas is arranged, the partner will"
			  " be aligned as if there was an edge from this node to its"
			  " partner."),
			LANV_TYPE_NODE,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_LABEL, g_param_spec_string(
			"label",
			_("Label"),
			_("The text to display as a label on this node."),
			NULL,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_SHOW_LABEL, g_param_spec_boolean(
			"show-label",
			_("Show label"),
			_("Whether or not to show the label."),
			0,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_DASH_LENGTH, g_param_spec_double(
			"dash-length",
			_("Border dash length"),
			_("Length of border dashes, or zero for no dashing."),
			0.0, G_MAXDOUBLE,
			0.0,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_DASH_OFFSET, g_param_spec_double(
			"dash-offset",
			_("Border dash offset"),
			_("Start offset for border dashes, used for selected animation."),
			0.0, G_MAXDOUBLE,
			0.0,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_BORDER_WIDTH, g_param_spec_double(
			"border-width",
			_("Border width"),
			_("Width of the border line."),
			0.0, G_MAXDOUBLE,
			2.0,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_FILL_COLOR, g_param_spec_uint(
			"fill-color",
			_("Fill color"),
			_("Color of internal area."),
			0, G_MAXUINT,
			DEFAULT_FILL_COLOR,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_BORDER_COLOR, g_param_spec_uint(
			"border-color",
			_("Border color"),
			_("Color of border line."),
			0, G_MAXUINT,
			DEFAULT_BORDER_COLOR,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_CAN_TAIL, g_param_spec_boolean(
			"can-tail",
			_("Can tail"),
			_("Whether this node can be the tail of an edge."),
			0,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_CAN_HEAD, g_param_spec_boolean(
			"can-head",
			_("Can head"),
			_("Whether this object can be the head of an edge."),
			0,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_IS_SOURCE, g_param_spec_boolean(
			"is-source",
			_("Is source"),
			_("Whether this object should be positioned at the start of signal flow."),
			0,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_SELECTED, g_param_spec_boolean(
			"selected",
			_("Selected"),
			_("Whether this object is selected."),
			0,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_HIGHLIGHTED, g_param_spec_boolean(
			"highlighted",
			_("Highlighted"),
			_("Whether this object is highlighted."),
			0,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_DRAGGABLE, g_param_spec_boolean(
			"draggable",
			_("Draggable"),
			_("Whether this object is draggable."),
			0,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_GRABBED, g_param_spec_boolean(
			"grabbed",
			_("Grabbed"),
			_("Whether this object is grabbed by the user."),
			0,
			G_PARAM_READWRITE));

#if LANV_GIR_ENABLED		// FIXME
	signal_moved = g_signal_new("moved",
	                            lanv_node_get_type(),
	                            G_SIGNAL_RUN_FIRST,
	                            0, NULL, NULL,
	                            lanv_marshal_VOID__DOUBLE_DOUBLE,
	                            G_TYPE_NONE,
	                            2,
	                            G_TYPE_DOUBLE,
	                            G_TYPE_DOUBLE,
	                            0);
#endif

	object_class->destroy = lanv_node_destroy;

	item_class->realize = lanv_node_realize;
	item_class->event   = lanv_node_default_event;
	item_class->update  = lanv_node_update;
	item_class->draw    = lanv_node_draw;

	klass->disconnect  = lanv_node_default_disconnect;
	klass->move        = lanv_node_default_move;
	klass->move_to     = lanv_node_default_move_to;
	klass->resize      = lanv_node_default_resize;
	klass->redraw_text = lanv_node_default_redraw_text;
	klass->tick        = lanv_node_default_tick;
	klass->tail_vector = lanv_node_default_tail_vector;
	klass->head_vector = lanv_node_default_head_vector;
}

gboolean
lanv_node_can_tail(const LanvNode* self)
{
	return self->impl->can_tail;
}

gboolean
lanv_node_can_head(const LanvNode* self)
{
	return self->impl->can_head;
}

void
lanv_node_set_is_source(const LanvNode* node, gboolean is_source)
{
	node->impl->is_source = is_source;
}

gboolean
lanv_node_is_within(const LanvNode* node,
                    double          x1,
                    double          y1,
                    double          x2,
                    double          y2)
{
	return LANV_NODE_GET_CLASS(node)->is_within(node, x1, y1, x2, y2);
}

void
lanv_node_tick(LanvNode* node,
               double    seconds)
{
	LanvNodeClass* klass = LANV_NODE_GET_CLASS(node);
	if (klass->tick) {
		klass->tick(node, seconds);
	}
}

void
lanv_node_tail_vector(const LanvNode* self,
                      const LanvNode* head,
                      double*         x1,
                      double*         y1,
                      double*         x2,
                      double*         y2)
{
	LANV_NODE_GET_CLASS(self)->tail_vector(
		self, head, x1, y1, x2, y2);
}

void
lanv_node_head_vector(const LanvNode* self,
                      const LanvNode* tail,
                      double*         x1,
                      double*         y1,
                      double*         x2,
                      double*         y2)
{
	LANV_NODE_GET_CLASS(self)->head_vector(
		self, tail, x1, y1, x2, y2);
}

const char*
lanv_node_get_label(const LanvNode* node)
{
	return node->impl->label ? node->impl->label->impl->text : NULL;
}

double
lanv_node_get_border_width(const LanvNode* node)
{
	return node->impl->border_width;
}

void
lanv_node_set_border_width(const LanvNode* node, double border_width)
{
	node->impl->border_width = border_width;
	lanv_item_request_update(LANV_ITEM(node));
}

double
lanv_node_get_dash_length(const LanvNode* node)
{
	return node->impl->dash_length;
}

void
lanv_node_set_dash_length(const LanvNode* node, double dash_length)
{
	node->impl->dash_length = dash_length;
	lanv_item_request_update(LANV_ITEM(node));
}

double
lanv_node_get_dash_offset(const LanvNode* node)
{
	return node->impl->dash_offset;
}

void
lanv_node_set_dash_offset(const LanvNode* node, double dash_offset)
{
	node->impl->dash_offset = dash_offset;
	lanv_item_request_update(LANV_ITEM(node));
}

guint
lanv_node_get_fill_color(const LanvNode* node)
{
	return node->impl->fill_color;
}

void
lanv_node_set_fill_color(const LanvNode* node, guint fill_color)
{
	node->impl->fill_color = fill_color;
	lanv_item_request_update(LANV_ITEM(node));
}

guint
lanv_node_get_border_color(const LanvNode* node)
{
	return node->impl->border_color;
}

void
lanv_node_set_border_color(const LanvNode* node, guint border_color)
{
	node->impl->border_color = border_color;
	lanv_item_request_update(LANV_ITEM(node));
}

LanvNode*
lanv_node_get_partner(const LanvNode* node)
{
	return node->impl->partner;
}

void
lanv_node_move(LanvNode* node,
               double    dx,
               double    dy)
{
	LANV_NODE_GET_CLASS(node)->move(node, dx, dy);
}

void
lanv_node_move_to(LanvNode* node,
                  double    x,
                  double    y)
{
	LANV_NODE_GET_CLASS(node)->move_to(node, x, y);
}

void
lanv_node_resize(LanvNode* node)
{
	LANV_NODE_GET_CLASS(node)->resize(node);
	node->impl->must_resize = FALSE;
}

void
lanv_node_redraw_text(LanvNode* node)
{
	LANV_NODE_GET_CLASS(node)->redraw_text(node);
}

void
lanv_node_disconnect(LanvNode* node)
{
	LANV_NODE_GET_CLASS(node)->disconnect(node);
}

gboolean
lanv_node_is_selected(LanvNode* node)
{
	gboolean selected = FALSE;
	g_object_get(node, "selected", &selected, NULL);
	return selected;
}

#endif
