/* This file is part of Lanv.
 * Copyright 2007-2016 David Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2025 Nedko Arnaudov
 *
 * Lanv is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Lanv is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Lanv.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "boilerplate.h"
#include "color.h"
#include "lanv-private.h"
#include "gettext.h"

#include <lanv/canvas.h>
#include <lanv/edge.h>
#include <lanv/item.h>
#include <lanv/node.h>
#include <lanv/types.h>

#include <cairo.h>

#if 0
#include <glib-object.h>
#include <glib.h>
#include <gtk/gtk.h>
#endif

#include <stdarg.h>

#include <math.h>
#include <string.h>

#define ARROW_DEPTH   32
#define ARROW_BREADTH 32

// Uncomment to see control point path as straight lines
//#define LANV_DEBUG_CURVES 1

// Uncomment along with LANV_DEBUG_CURVES to see bounding box (buggy)
//#define LANV_DEBUG_BOUNDS 1

#if 0
enum {
	PROP_0,
	PROP_TAIL,
	PROP_HEAD,
	PROP_WIDTH,
	PROP_HANDLE_RADIUS,
	PROP_DASH_LENGTH,
	PROP_DASH_OFFSET,
	PROP_COLOR,
	PROP_CONSTRAINING,
	PROP_CURVED,
	PROP_ARROWHEAD,
	PROP_SELECTED,
	PROP_HIGHLIGHTED,
	PROP_GHOST
};

G_DEFINE_TYPE_WITH_CODE(LanvEdge, lanv_edge, LANV_TYPE_ITEM,
                        G_ADD_PRIVATE(LanvEdge))

static LanvItemClass* parent_class;

static void
lanv_edge_init(LanvEdge* edge)
{
	LanvEdgePrivate* impl =
	    (LanvEdgePrivate*)lanv_edge_get_instance_private(edge);

	edge->impl = impl;

	impl->tail = NULL;
	impl->head = NULL;

	memset(&impl->coords, '\0', sizeof(LanvEdgeCoords));
	impl->coords.width         = 2.0;
	impl->coords.handle_radius = 4.0;
	impl->coords.constraining  = TRUE;
	impl->coords.curved        = FALSE;
	impl->coords.arrowhead     = FALSE;

	impl->old_coords  = impl->coords;
	impl->dash_length = 0.0;
	impl->dash_offset = 0.0;
	impl->color       = 0;
}

static void
lanv_edge_destroy(GtkObject* object)
{
	g_return_if_fail(object != NULL);
	g_return_if_fail(LANV_IS_EDGE(object));

	LanvEdge*   edge   = LANV_EDGE(object);
	LanvCanvas* canvas = LANV_CANVAS(edge->item.impl->canvas);
	if (canvas && !edge->impl->ghost) {
		edge->item.impl->canvas = NULL;
	}
	edge->item.impl->parent = NULL;

	if (GTK_OBJECT_CLASS(parent_class)->destroy) {
		(*GTK_OBJECT_CLASS(parent_class)->destroy)(object);
	}
}

static void
lanv_edge_set_property(GObject*      object,
                       guint         prop_id,
                       const GValue* value,
                       GParamSpec*   pspec)
{
	g_return_if_fail(object != NULL);
	g_return_if_fail(LANV_IS_EDGE(object));

	LanvItem*        item   = LANV_ITEM(object);
	LanvEdge*        edge   = LANV_EDGE(object);
	LanvEdgePrivate* impl   = edge->impl;
	LanvEdgeCoords*  coords = &impl->coords;

	switch (prop_id) {
		SET_CASE(WIDTH, double, coords->width)
		SET_CASE(HANDLE_RADIUS, double, coords->handle_radius)
		SET_CASE(DASH_LENGTH, double, impl->dash_length)
		SET_CASE(DASH_OFFSET, double, impl->dash_offset)
		SET_CASE(COLOR, uint, impl->color)
		SET_CASE(CONSTRAINING, boolean, impl->coords.constraining)
		SET_CASE(CURVED, boolean, impl->coords.curved)
		SET_CASE(ARROWHEAD, boolean, impl->coords.arrowhead)
		SET_CASE(SELECTED, boolean, impl->selected)
		SET_CASE(HIGHLIGHTED, boolean, impl->highlighted)
		SET_CASE(GHOST, boolean, impl->ghost)
	case PROP_TAIL: {
		const gobject tmp = g_value_get_object(value);
		if (impl->tail != tmp) {
			impl->tail = LANV_NODE(tmp);
			lanv_item_request_update(item);
		}
		break;
	}
	case PROP_HEAD: {
		const gobject tmp = g_value_get_object(value);
		if (impl->head != tmp) {
			impl->head = LANV_NODE(tmp);
			lanv_item_request_update(item);
		}
		break;
	}
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

static void
lanv_edge_get_property(GObject*    object,
                       guint       prop_id,
                       GValue*     value,
                       GParamSpec* pspec)
{
	g_return_if_fail(object != NULL);
	g_return_if_fail(LANV_IS_EDGE(object));

	LanvEdge*        edge = LANV_EDGE(object);
	LanvEdgePrivate* impl = edge->impl;

	switch (prop_id) {
		GET_CASE(TAIL, object, impl->tail)
		GET_CASE(HEAD, object, impl->head)
		GET_CASE(WIDTH, double, impl->coords.width)
		SET_CASE(HANDLE_RADIUS, double, impl->coords.handle_radius)
		GET_CASE(DASH_LENGTH, double, impl->dash_length)
		GET_CASE(DASH_OFFSET, double, impl->dash_offset)
		GET_CASE(COLOR, uint, impl->color)
		GET_CASE(CONSTRAINING, boolean, impl->coords.constraining)
		GET_CASE(CURVED, boolean, impl->coords.curved)
		GET_CASE(ARROWHEAD, boolean, impl->coords.arrowhead)
		GET_CASE(SELECTED, boolean, impl->selected)
		GET_CASE(HIGHLIGHTED, boolean, impl->highlighted)
		SET_CASE(GHOST, boolean, impl->ghost)
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

void
lanv_edge_request_redraw(LanvItem*             item,
                         const LanvEdgeCoords* coords)
{
	LanvCanvas* canvas = item->impl->canvas;
	const double w = coords->width;
	if (coords->curved) {
		const double src_x  = coords->x1;
		const double src_y  = coords->y1;
		const double dst_x  = coords->x2;
		const double dst_y  = coords->y2;
		const double join_x = (src_x + dst_x) / 2.0;
		const double join_y = (src_y + dst_y) / 2.0;
		const double src_x1 = coords->cx1;
		const double src_y1 = coords->cy1;
		const double dst_x1 = coords->cx2;
		const double dst_y1 = coords->cy2;

		const double r1x1 = MIN(MIN(src_x, join_x), src_x1);
		const double r1y1 = MIN(MIN(src_y, join_y), src_y1);
		const double r1x2 = MAX(MAX(src_x, join_x), src_x1);
		const double r1y2 = MAX(MAX(src_y, join_y), src_y1);
		lanv_canvas_request_redraw_w(canvas,
		                             r1x1 - w, r1y1 - w,
		                             r1x2 + w, r1y2 + w);

		const double r2x1 = MIN(MIN(dst_x, join_x), dst_x1);
		const double r2y1 = MIN(MIN(dst_y, join_y), dst_y1);
		const double r2x2 = MAX(MAX(dst_x, join_x), dst_x1);
		const double r2y2 = MAX(MAX(dst_y, join_y), dst_y1);
		lanv_canvas_request_redraw_w(canvas,
		                             r2x1 - w, r2y1 - w,
		                             r2x2 + w, r2y2 + w);

	} else {
		const double x1 = MIN(coords->x1, coords->x2);
		const double y1 = MIN(coords->y1, coords->y2);
		const double x2 = MAX(coords->x1, coords->x2);
		const double y2 = MAX(coords->y1, coords->y2);

		lanv_canvas_request_redraw_w(canvas,
		                             x1 - w, y1 - w,
		                             x2 + w, y2 + w);
	}

	if (coords->handle_radius > 0.0) {
		lanv_canvas_request_redraw_w(
			canvas,
			coords->handle_x - coords->handle_radius - w,
			coords->handle_y - coords->handle_radius - w,
			coords->handle_x + coords->handle_radius + w,
			coords->handle_y + coords->handle_radius + w);
	}

	if (coords->arrowhead) {
		lanv_canvas_request_redraw_w(
			canvas,
			coords->x2 - ARROW_DEPTH,
			coords->y2 - ARROW_BREADTH,
			coords->x2 + ARROW_DEPTH,
			coords->y2 + ARROW_BREADTH);
	}
}

static void
lanv_edge_bounds(LanvItem* item,
                 double* x1, double* y1,
                 double* x2, double* y2)
{
	LanvEdge*        edge   = LANV_EDGE(item);
	LanvEdgePrivate* impl   = edge->impl;
	LanvEdgeCoords*  coords = &impl->coords;
	const double     w      = coords->width;

	if (coords->curved) {
		*x1 = MIN(coords->x1, MIN(coords->cx1, MIN(coords->x2, coords->cx2))) - w;
		*y1 = MIN(coords->y1, MIN(coords->cy1, MIN(coords->y2, coords->cy2))) - w;
		*x2 = MAX(coords->x1, MAX(coords->cx1, MAX(coords->x2, coords->cx2))) + w;
		*y2 = MAX(coords->y1, MAX(coords->cy1, MAX(coords->y2, coords->cy2))) + w;
	} else {
		*x1 = MIN(impl->coords.x1, impl->coords.x2) - w;
		*y1 = MIN(impl->coords.y1, impl->coords.y2) - w;
		*x2 = MAX(impl->coords.x1, impl->coords.x2) + w;
		*y2 = MAX(impl->coords.y1, impl->coords.y2) + w;
	}
}

void
lanv_edge_get_coords(const LanvEdge* edge, LanvEdgeCoords* coords)
{
	LanvEdgePrivate* impl = edge->impl;

	LANV_NODE_GET_CLASS(impl->tail)->tail_vector(
		impl->tail, impl->head,
		&coords->x1, &coords->y1, &coords->cx1, &coords->cy1);
	LANV_NODE_GET_CLASS(impl->head)->head_vector(
		impl->head, impl->tail,
		&coords->x2, &coords->y2, &coords->cx2, &coords->cy2);

	const double dx = coords->x2 - coords->x1;
	const double dy = coords->y2 - coords->y1;

	coords->handle_x = coords->x1 + (dx / 2.0);
	coords->handle_y = coords->y1 + (dy / 2.0);

	const double abs_dx = fabs(dx);
	const double abs_dy = fabs(dy);

	coords->cx1 = coords->x1 + (coords->cx1 * (abs_dx / 4.0));
	coords->cy1 = coords->y1 + (coords->cy1 * (abs_dy / 4.0));
	coords->cx2 = coords->x2 + (coords->cx2 * (abs_dx / 4.0));
	coords->cy2 = coords->y2 + (coords->cy2 * (abs_dy / 4.0));
}

static void
lanv_edge_update(LanvItem* item, int flags)
{
	LanvEdge*        edge = LANV_EDGE(item);
	LanvEdgePrivate* impl = edge->impl;

	// Request redraw of old location
	lanv_edge_request_redraw(item, &impl->old_coords);

	// Calculate new coordinates from tail and head
	lanv_edge_get_coords(edge, &impl->coords);

	// Update old coordinates
	impl->old_coords = impl->coords;

	// Get bounding box
	double x1 = 0.0;
	double x2 = 0.0;
	double y1 = 0.0;
	double y2 = 0.0;
	lanv_edge_bounds(item, &x1, &y1, &x2, &y2);

	// Ensure bounding box has non-zero area
	if (x1 == x2) {
		x2 += 1.0;
	}
	if (y1 == y2) {
		y2 += 1.0;
	}

	// Update world-relative bounding box
	item->impl->x1 = x1;
	item->impl->y1 = y1;
	item->impl->x2 = x2;
	item->impl->y2 = y2;
	lanv_item_i2w_pair(item, &item->impl->x1, &item->impl->y1, &item->impl->x2, &item->impl->y2);

	// Request redraw of new location
	lanv_edge_request_redraw(item, &impl->coords);

	parent_class->update(item, flags);
}

static void
lanv_edge_draw(LanvItem* item,
               cairo_t* cr, double cx, double cy, double cw, double ch)
{
	(void)cx;
	(void)cy;
	(void)cw;
	(void)ch;

	LanvEdge*        edge = LANV_EDGE(item);
	LanvEdgePrivate* impl = edge->impl;

	double src_x = impl->coords.x1;
	double src_y = impl->coords.y1;
	double dst_x = impl->coords.x2;
	double dst_y = impl->coords.y2;
	double dx    = src_x - dst_x;
	double dy    = src_y - dst_y;

	double r = 0.0;
	double g = 0.0;
	double b = 0.0;
	double a = 0.0;
	if (impl->highlighted) {
		color_to_rgba(highlight_color(impl->color, 0x40), &r, &g, &b, &a);
	} else {
		color_to_rgba(impl->color, &r, &g, &b, &a);
	}
	cairo_set_source_rgba(cr, r, g, b, a);

	cairo_set_line_width(cr, impl->coords.width);
	cairo_move_to(cr, src_x, src_y);

	const double dash_length = (impl->selected ? 4.0 : impl->dash_length);
	if (dash_length > 0.0) {
		double dashed[2] = { dash_length, dash_length };
		cairo_set_dash(cr, dashed, 2, impl->dash_offset);
	} else {
		cairo_set_dash(cr, &dash_length, 0, 0);
	}

	const double join_x = (src_x + dst_x) / 2.0;
	const double join_y = (src_y + dst_y) / 2.0;

	if (impl->coords.curved) {
		// Curved line as 2 paths which join at the middle point

		// Path 1 (src_x, src_y) -> (join_x, join_y)
		// Control point 1
		const double src_x1 = impl->coords.cx1;
		const double src_y1 = impl->coords.cy1;
		// Control point 2
		const double src_x2 = (join_x + src_x1) / 2.0;
		const double src_y2 = (join_y + src_y1) / 2.0;

		// Path 2, (join_x, join_y) -> (dst_x, dst_y)
		// Control point 1
		const double dst_x1 = impl->coords.cx2;
		const double dst_y1 = impl->coords.cy2;
		// Control point 2
		const double dst_x2 = (join_x + dst_x1) / 2.0;
		const double dst_y2 = (join_y + dst_y1) / 2.0;

		cairo_move_to(cr, src_x, src_y);
		cairo_curve_to(cr, src_x1, src_y1, src_x2, src_y2, join_x, join_y);
		cairo_curve_to(cr, dst_x2, dst_y2, dst_x1, dst_y1, dst_x, dst_y);

#ifdef LANV_DEBUG_CURVES
		cairo_stroke(cr);
		cairo_save(cr);
		cairo_set_source_rgba(cr, 1.0, 0, 0, 0.5);

		cairo_move_to(cr, src_x, src_y);
		cairo_line_to(cr, src_x1, src_y1);
		cairo_stroke(cr);

		cairo_move_to(cr, join_x, join_y);
		cairo_line_to(cr, src_x2, src_y2);
		cairo_stroke(cr);

		cairo_move_to(cr, join_x, join_y);
		cairo_line_to(cr, dst_x2, dst_y2);
		cairo_stroke(cr);

		cairo_move_to(cr, dst_x, dst_y);
		cairo_line_to(cr, dst_x1, dst_y1);
		cairo_stroke(cr);

#ifdef LANV_DEBUG_BOUNDS
		double bounds_x1, bounds_y1, bounds_x2, bounds_y2;
		lanv_edge_bounds(item, &bounds_x1, &bounds_y1, &bounds_x2, &bounds_y2);
		cairo_rectangle(cr,
		                bounds_x1, bounds_y1,
		                bounds_x2 - bounds_x1, bounds_y2 - bounds_y1);
#endif

		cairo_restore(cr);
#endif

		cairo_stroke(cr);
		if (impl->coords.arrowhead) {
			cairo_move_to(cr, dst_x - 12, dst_y - 4);
			cairo_line_to(cr, dst_x, dst_y);
			cairo_line_to(cr, dst_x - 12, dst_y + 4);
			cairo_close_path(cr);
			cairo_stroke_preserve(cr);
			cairo_fill(cr);
		}

	} else {
		// Straight line from (x1, y1) to (x2, y2)
		cairo_move_to(cr, src_x, src_y);
		cairo_line_to(cr, dst_x, dst_y);
		cairo_stroke(cr);

		if (impl->coords.arrowhead) {
			const double ah  = sqrt(dx * dx + dy * dy);
			const double adx = dx / ah * 8.0;
			const double ady = dy / ah * 8.0;

			cairo_move_to(cr,
			              dst_x + adx - ady/1.5,
			              dst_y + ady + adx/1.5);
			cairo_set_line_join(cr, CAIRO_LINE_JOIN_BEVEL);
			cairo_line_to(cr, dst_x, dst_y);
			cairo_set_line_join(cr, CAIRO_LINE_JOIN_MITER);
			cairo_line_to(cr,
			              dst_x + adx + ady/1.5,
			              dst_y + ady - adx/1.5);
			cairo_close_path(cr);
			cairo_stroke_preserve(cr);
			cairo_fill(cr);
		}
	}

	if (!lanv_canvas_exporting(item->impl->canvas) &&
	    impl->coords.handle_radius > 0.0) {
		cairo_move_to(cr, join_x, join_y);
		cairo_arc(cr, join_x, join_y, impl->coords.handle_radius, 0, 2 * G_PI);
		cairo_fill(cr);
	}
}

static double
lanv_edge_point(LanvItem* item, double x, double y, LanvItem** actual_item)
{
	const LanvEdge*       edge = LANV_EDGE(item);
	const LanvEdgeCoords* coords = &edge->impl->coords;

	const double dx = fabs(x - coords->handle_x);
	const double dy = fabs(y - coords->handle_y);
	const double d  = sqrt((dx * dx) + (dy * dy));

	*actual_item = item;

	if (d <= coords->handle_radius) {
		// Point is inside the handle
		return 0.0;
	} else {
		// Distance from the edge of the handle
		return d - (coords->handle_radius + coords->width);
	}
}

gboolean
lanv_edge_is_within(const LanvEdge* edge,
                    double          x1,
                    double          y1,
                    double          x2,
                    double          y2)
{
	const double handle_x = edge->impl->coords.handle_x;
	const double handle_y = edge->impl->coords.handle_y;

	return handle_x >= x1
		&& handle_x <= x2
		&& handle_y >= y1
		&& handle_y <= y2;
}

static void
lanv_edge_class_init(LanvEdgeClass* klass)
{
	GObjectClass*   gobject_class = (GObjectClass*)klass;
	GtkObjectClass* object_class  = (GtkObjectClass*)klass;
	LanvItemClass*  item_class    = (LanvItemClass*)klass;

	parent_class = LANV_ITEM_CLASS(g_type_class_peek_parent(klass));

	gobject_class->set_property = lanv_edge_set_property;
	gobject_class->get_property = lanv_edge_get_property;

	g_object_class_install_property(
		gobject_class, PROP_TAIL, g_param_spec_object(
			"tail",
			_("Tail"),
			_("Node this edge starts from."),
			LANV_TYPE_NODE,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_HEAD, g_param_spec_object(
			"head",
			_("Head"),
			_("Node this edge ends at."),
			LANV_TYPE_NODE,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_WIDTH, g_param_spec_double(
			"width",
			_("Line width"),
			_("Width of edge line."),
			0.0, G_MAXDOUBLE,
			2.0,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_HANDLE_RADIUS, g_param_spec_double(
			"handle-radius",
			_("Gandle radius"),
			_("Radius of handle in canvas units."),
			0.0, G_MAXDOUBLE,
			4.0,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_DASH_LENGTH, g_param_spec_double(
			"dash-length",
			_("Line dash length"),
			_("Length of line dashes, or zero for no dashing."),
			0.0, G_MAXDOUBLE,
			0.0,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_DASH_OFFSET, g_param_spec_double(
			"dash-offset",
			_("Line dash offset"),
			_("Start offset for line dashes, used for selected animation."),
			0.0, G_MAXDOUBLE,
			0.0,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_COLOR, g_param_spec_uint(
			"color",
			_("Color"),
			_("Line color as an RGBA integer."),
			0, G_MAXUINT,
			0,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_CONSTRAINING, g_param_spec_boolean(
			"constraining",
			_("Constraining"),
			_("Whether edge should constrain the layout."),
			1,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_CURVED, g_param_spec_boolean(
			"curved",
			_("Curved"),
			_("Whether line should be curved rather than straight."),
			0,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_ARROWHEAD, g_param_spec_boolean(
			"arrowhead",
			_("Arrowhead"),
			_("Whether to show an arrowhead at the head of this edge."),
			0,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_SELECTED, g_param_spec_boolean(
			"selected",
			_("Selected"),
			_("Whether this edge is selected."),
			0,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_HIGHLIGHTED, g_param_spec_boolean(
			"highlighted",
			_("Highlighted"),
			_("Whether to highlight the edge."),
			0,
			G_PARAM_READWRITE));

	g_object_class_install_property(
		gobject_class, PROP_GHOST, g_param_spec_boolean(
			"ghost",
			_("Ghost"),
			_("Whether this edge is a `ghost', which is an edge that is not "
			  "added to the canvas data structures.  Ghost edges are used for "
			  "temporary edges that are not considered `real', e.g. the edge "
			  "made while dragging to make a connection."),
			0,
			G_PARAM_READWRITE));

	object_class->destroy = lanv_edge_destroy;

	item_class->update = lanv_edge_update;
	item_class->bounds = lanv_edge_bounds;
	item_class->point  = lanv_edge_point;
	item_class->draw   = lanv_edge_draw;
}

LanvEdge*
lanv_edge_new(LanvCanvas* canvas,
              LanvNode*   tail,
              LanvNode*   head,
              const char* first_prop_name, ...)
{
	LanvEdge* edge = LANV_EDGE(
		g_object_new(lanv_edge_get_type(), NULL));

	va_list args;
	va_start(args, first_prop_name);
	lanv_item_construct(&edge->item,
	                    LANV_ITEM(lanv_canvas_root(canvas)),
	                    first_prop_name, args);
	va_end(args);

	edge->impl->tail = tail;
	edge->impl->head = head;

	if (!edge->impl->color) {
		const guint tail_color = LANV_NODE(tail)->impl->fill_color;
		g_object_set(G_OBJECT(edge),
		             "color", EDGE_COLOR(tail_color),
		             NULL);
	}

	if (!edge->impl->ghost) {
		lanv_canvas_add_edge(canvas, edge);
	}
	return edge;
}

void
lanv_edge_update_location(LanvEdge* edge)
{
	lanv_item_request_update(LANV_ITEM(edge));
}

gboolean
lanv_edge_get_constraining(const LanvEdge* edge)
{
	return edge->impl->coords.constraining;
}

void
lanv_edge_set_constraining(LanvEdge* edge, gboolean constraining)
{
	edge->impl->coords.constraining = constraining;
	lanv_edge_request_redraw(LANV_ITEM(edge), &edge->impl->coords);
}

gboolean
lanv_edge_get_curved(const LanvEdge* edge)
{
	return edge->impl->coords.curved;
}

void
lanv_edge_set_curved(LanvEdge* edge, gboolean curved)
{
	edge->impl->coords.curved = curved;
	lanv_edge_request_redraw(LANV_ITEM(edge), &edge->impl->coords);
}

void
lanv_edge_set_selected(LanvEdge* edge, gboolean selected)
{
	LanvCanvas* canvas = LANV_CANVAS(edge->item.impl->canvas);
	if (selected) {
		lanv_canvas_select_edge(canvas, edge);
	} else {
		lanv_canvas_unselect_edge(canvas, edge);
	}
}

void
lanv_edge_select(LanvEdge* edge)
{
	lanv_edge_set_selected(edge, TRUE);
}

void
lanv_edge_unselect(LanvEdge* edge)
{
	lanv_edge_set_selected(edge, FALSE);
}

void
lanv_edge_highlight(LanvEdge* edge)
{
	lanv_edge_set_highlighted(edge, TRUE);
}

void
lanv_edge_unhighlight(LanvEdge* edge)
{
	lanv_edge_set_highlighted(edge, FALSE);
}

void
lanv_edge_set_highlighted(LanvEdge* edge, gboolean highlighted)
{
	edge->impl->highlighted = highlighted;
	lanv_edge_request_redraw(LANV_ITEM(edge), &edge->impl->coords);
}

void
lanv_edge_tick(LanvEdge* edge, double seconds)
{
	lanv_item_set(LANV_ITEM(edge),
	              "dash-offset", seconds * 8.0,
	              NULL);
}

void
lanv_edge_disconnect(LanvEdge* edge)
{
	if (!edge->impl->ghost) {
		lanv_canvas_disconnect_edge(
			LANV_CANVAS(edge->item.impl->canvas),
			edge);
	}
}

void
lanv_edge_remove(LanvEdge* edge)
{
	if (!edge->impl->ghost) {
		lanv_canvas_remove_edge(
			LANV_CANVAS(edge->item.impl->canvas),
			edge);
	}
}

LanvNode*
lanv_edge_get_tail(const LanvEdge* edge)
{
	return edge->impl->tail;
}

LanvNode*
lanv_edge_get_head(const LanvEdge* edge)
{
	return edge->impl->head;
}
#endif
