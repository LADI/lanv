/* This file is part of Lanv.
 * Copyright 2007-2013 David Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2025 Nedko Arnaudov
 *
 * Lanv is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Lanv is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Lanv.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LANV_GETTEXT_H
#define LANV_GETTEXT_H

#ifdef ENABLE_NLS
#    include <libintl.h>
#    define _(str) dgettext("lanv", str)
#else
#    define _(str) str
#endif

#endif  /* LANV_GETTEXT_H */
