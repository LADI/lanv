/* This file is part of Lanv.
 * Copyright 2007-2016 David Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2025 Nedko Arnaudov
 *
 * Lanv is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Lanv is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Lanv.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Based on GnomeCanvas, by Federico Mena <federico@nuclecu.unam.mx>
 * and Raph Levien <raph@gimp.org>
 * Copyright 1997-2000 Free Software Foundation
 */

#if LANV_GIR_ENABLED
#include "lanv-marshal.h"
#endif
#include "lanv-private.h"
#include "gettext.h"

#include <lanv/item.h>
#include <lanv/types.h>

#if 0
#include <gdk/gdk.h>
#include <glib-object.h>
#include <glib.h>
#include <gtk/gtk.h>

#include <stdarg.h>
#include <stdio.h>

/* All canvas items are derived from LanvItem.  The only information a LanvItem
 * contains is its parent canvas, its parent canvas item, its bounding box in
 * world coordinates, and its current affine transformation.
 *
 * Items inside a canvas are organized in a tree, where leaves are items
 * without any children.  Each canvas has a single root item, which can be
 * obtained with the lanv_canvas_base_get_root() function.
 *
 * The abstract LanvItem class does not have any configurable or queryable
 * attributes.
 */

/* Update flags for items */
enum {
	LANV_CANVAS_UPDATE_REQUESTED  = 1 << 0,
	LANV_CANVAS_UPDATE_AFFINE     = 1 << 1,
	LANV_CANVAS_UPDATE_VISIBILITY = 1 << 2
};

#define GCI_UPDATE_MASK (LANV_CANVAS_UPDATE_REQUESTED \
                         | LANV_CANVAS_UPDATE_AFFINE \
                         | LANV_CANVAS_UPDATE_VISIBILITY)

enum {
	ITEM_PROP_0,
	ITEM_PROP_PARENT,
	ITEM_PROP_X,
	ITEM_PROP_Y,
	ITEM_PROP_MANAGED
};

enum {
	ITEM_EVENT,
	ITEM_LAST_SIGNAL
};

static guint item_signals[ITEM_LAST_SIGNAL];

G_DEFINE_TYPE_WITH_CODE(LanvItem, lanv_item, GTK_TYPE_OBJECT,
                        G_ADD_PRIVATE(LanvItem))

static GtkObjectClass* item_parent_class;

/* Object initialization function for LanvItem */
static void
lanv_item_init(LanvItem* item)
{
	LanvItemPrivate* impl =
	    (LanvItemPrivate*)lanv_item_get_instance_private(item);

	item->object.flags |= LANV_ITEM_VISIBLE;
	item->impl          = impl;
	item->impl->managed = FALSE;
	item->impl->wrapper = NULL;
}

/**
 * lanv_item_new:
 * @parent: The parent group for the new item.
 * @type: The object type of the item.
 * @first_arg_name: A list of object argument name/value pairs, NULL-terminated,
 * used to configure the item.  For example, "fill_color", "black",
 * "width_units", 5.0, NULL.
 * @...: first argument value, second argument name, second argument value, ...
 *
 * Creates a new canvas item with @parent as its parent group.  The item is
 * created at the top of its parent's stack, and starts up as visible.  The item
 * is of the specified @type, for example, it can be
 * lanv_canvas_rect_get_type().  The list of object arguments/value pairs is
 * used to configure the item. If you need to pass construct time parameters, you
 * should use g_object_new() to pass the parameters and
 * lanv_item_construct() to set up the canvas item.
 *
 * Return value: (transfer full): The newly-created item.
 **/
LanvItem*
lanv_item_new(LanvItem* parent, GType type, const gchar* first_arg_name, ...)
{
	g_return_val_if_fail(g_type_is_a(type, lanv_item_get_type()), NULL);

	LanvItem* item = LANV_ITEM(g_object_new(type, NULL));

	va_list args;
	va_start(args, first_arg_name);
	lanv_item_construct(item, parent, first_arg_name, args);
	va_end(args);

	return item;
}

/* Performs post-creation operations on a canvas item (adding it to its parent
 * group, etc.)
 */
static void
item_post_create_setup(LanvItem* item)
{
	LanvItemClass* parent_class = LANV_ITEM_GET_CLASS(item->impl->parent);
	if (!item->impl->managed) {
		if (parent_class->add) {
			parent_class->add(item->impl->parent, item);
		} else {
			g_warning("item added to non-parent item\n");
		}
	}
	lanv_canvas_request_redraw_w(item->impl->canvas,
	                             item->impl->x1, item->impl->y1,
	                             item->impl->x2 + 1, item->impl->y2 + 1);
	lanv_canvas_set_need_repick(item->impl->canvas);
}

static void
lanv_item_set_property(GObject*      object,
                       guint         prop_id,
                       const GValue* value,
                       GParamSpec*   pspec)
{
	g_return_if_fail(object != NULL);
	g_return_if_fail(LANV_IS_ITEM(object));

	LanvItem* item = LANV_ITEM(object);

	switch (prop_id) {
	case ITEM_PROP_PARENT:
		if (item->impl->parent != NULL) {
			g_warning("Cannot set `parent' argument after item has "
			          "already been constructed.");
		} else if (g_value_get_object(value)) {
			item->impl->parent = LANV_ITEM(g_value_get_object(value));
			item->impl->canvas = item->impl->parent->impl->canvas;
			item_post_create_setup(item);
		}
		break;
	case ITEM_PROP_X:
		item->impl->x = g_value_get_double(value);
		lanv_item_request_update(item);
		break;
	case ITEM_PROP_Y:
		item->impl->y = g_value_get_double(value);
		lanv_item_request_update(item);
		break;
	case ITEM_PROP_MANAGED:
		item->impl->managed = g_value_get_boolean(value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

static void
lanv_item_get_property(GObject*    object,
                       guint       prop_id,
                       GValue*     value,
                       GParamSpec* pspec)
{
	g_return_if_fail(object != NULL);
	g_return_if_fail(LANV_IS_ITEM(object));

	LanvItem* item = LANV_ITEM(object);

	switch (prop_id) {
	case ITEM_PROP_PARENT:
		g_value_set_object(value, item->impl->parent);
		break;
	case ITEM_PROP_X:
		g_value_set_double(value, item->impl->x);
		break;
	case ITEM_PROP_Y:
		g_value_set_double(value, item->impl->y);
		break;
	case ITEM_PROP_MANAGED:
		g_value_set_boolean(value, item->impl->managed);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

/**
 * lanv_item_construct:
 * @item: An unconstructed canvas item.
 * @parent: The parent group for the item.
 * @first_arg_name: The name of the first argument for configuring the item.
 * @args: The list of arguments used to configure the item.
 *
 * Constructs a canvas item; meant for use only by item implementations.
 **/
void
lanv_item_construct(LanvItem* item, LanvItem* parent,
                    const gchar* first_arg_name, va_list args)
{
	g_return_if_fail(LANV_IS_ITEM(item));

	item->impl->parent  = parent;
	item->impl->wrapper = NULL;
	item->impl->canvas  = item->impl->parent->impl->canvas;
	item->impl->layer   = 0;

	g_object_set_valist(G_OBJECT(item), first_arg_name, args);

	item_post_create_setup(item);
}

/* If the item is visible, requests a redraw of it. */
static void
redraw_if_visible(LanvItem* item)
{
	if (item->object.flags & LANV_ITEM_VISIBLE) {
		lanv_canvas_request_redraw_w(item->impl->canvas,
		                             item->impl->x1, item->impl->y1,
		                             item->impl->x2 + 1, item->impl->y2 + 1);
	}
}

/* Standard object dispose function for canvas items */
static void
lanv_item_dispose(GObject* object)
{
	LanvItem* item = NULL;

	g_return_if_fail(LANV_IS_ITEM(object));

	item = LANV_ITEM(object);

	if (item->impl->canvas) {
		redraw_if_visible(item);
		lanv_canvas_forget_item(item->impl->canvas, item);
	}

	/* Normal destroy stuff */

	if (item->object.flags & LANV_ITEM_MAPPED) {
		(*LANV_ITEM_GET_CLASS(item)->unmap)(item);
	}

	if (item->object.flags & LANV_ITEM_REALIZED) {
		(*LANV_ITEM_GET_CLASS(item)->unrealize)(item);
	}

	if (!item->impl->managed && item->impl->parent) {
		if (LANV_ITEM_GET_CLASS(item->impl->parent)->remove) {
			LANV_ITEM_GET_CLASS(item->impl->parent)->remove(item->impl->parent, item);
		} else {
			fprintf(stderr, "warning: Item parent has no remove method\n");
		}
	}

	G_OBJECT_CLASS(item_parent_class)->dispose(object);
	/* items should remove any reference to item->impl->canvas after the
	   first ::destroy */
	item->impl->canvas = NULL;
}

/* Realize handler for canvas items */
static void
lanv_item_realize(LanvItem* item)
{
	GTK_OBJECT_SET_FLAGS(item, LANV_ITEM_REALIZED);

	lanv_item_request_update(item);
}

/* Unrealize handler for canvas items */
static void
lanv_item_unrealize(LanvItem* item)
{
	GTK_OBJECT_UNSET_FLAGS(item, LANV_ITEM_REALIZED);
}

/* Map handler for canvas items */
static void
lanv_item_map(LanvItem* item)
{
	GTK_OBJECT_SET_FLAGS(item, LANV_ITEM_MAPPED);
}

/* Unmap handler for canvas items */
static void
lanv_item_unmap(LanvItem* item)
{
	GTK_OBJECT_UNSET_FLAGS(item, LANV_ITEM_MAPPED);
}

/* Update handler for canvas items */
static void
lanv_item_update(LanvItem* item, int flags)
{
	(void)flags;

	GTK_OBJECT_UNSET_FLAGS(item, LANV_ITEM_NEED_UPDATE);
	GTK_OBJECT_UNSET_FLAGS(item, LANV_ITEM_NEED_VIS);
}

/* Point handler for canvas items */
static double
lanv_item_point(LanvItem* item, double x, double y, LanvItem** actual_item)
{
	(void)item;
	(void)x;
	(void)y;

	*actual_item = NULL;
	return G_MAXDOUBLE;
}

void
lanv_item_invoke_update(LanvItem* item, int flags)
{
	int child_flags = flags;

	/* apply object flags to child flags */

	child_flags &= ~LANV_CANVAS_UPDATE_REQUESTED;

	if (item->object.flags & LANV_ITEM_NEED_UPDATE) {
		child_flags |= LANV_CANVAS_UPDATE_REQUESTED;
	}

	if (item->object.flags & LANV_ITEM_NEED_VIS) {
		child_flags |= LANV_CANVAS_UPDATE_VISIBILITY;
	}

	if (child_flags & GCI_UPDATE_MASK) {
		if (LANV_ITEM_GET_CLASS(item)->update) {
			LANV_ITEM_GET_CLASS(item)->update(item, child_flags);
			g_assert(!(GTK_OBJECT_FLAGS(item) & LANV_ITEM_NEED_UPDATE));
		}
	}
}

/**
 * lanv_item_set:
 * @item: A canvas item.
 * @first_arg_name: The list of object argument name/value pairs used to configure the item.
 * @...: first argument value, second argument name, second argument value, ...
 *
 * Configures a canvas item.  The arguments in the item are set to the specified
 * values, and the item is repainted as appropriate.
 **/
void
lanv_item_set(LanvItem* item, const gchar* first_arg_name, ...)
{
	va_list args;

	va_start(args, first_arg_name);
	lanv_item_set_valist(item, first_arg_name, args);
	va_end(args);
}

/**
 * lanv_item_set_valist:
 * @item: A canvas item.
 * @first_arg_name: The name of the first argument used to configure the item.
 * @args: The list of object argument name/value pairs used to configure the item.
 *
 * Configures a canvas item.  The arguments in the item are set to the specified
 * values, and the item is repainted as appropriate.
 **/
void
lanv_item_set_valist(LanvItem* item, const gchar* first_arg_name, va_list args)
{
	g_return_if_fail(LANV_IS_ITEM(item));

	g_object_set_valist(G_OBJECT(item), first_arg_name, args);

	lanv_canvas_set_need_repick(item->impl->canvas);
}

LanvCanvas*
lanv_item_get_canvas(LanvItem* item)
{
	return item->impl->canvas;
}

LanvItem*
lanv_item_get_parent(LanvItem* item)
{
	return item->impl->parent;
}

void
lanv_item_raise(LanvItem* item)
{
	++item->impl->layer;
}

void
lanv_item_lower(LanvItem* item)
{
	--item->impl->layer;
}

/**
 * lanv_item_move:
 * @item: A canvas item.
 * @dx: Horizontal offset.
 * @dy: Vertical offset.
 **/
void
lanv_item_move(LanvItem* item, double dx, double dy)
{
	if (!item || !LANV_IS_ITEM(item)) {
		return;
	}

	item->impl->x += dx;
	item->impl->y += dy;

	lanv_item_request_update(item);
	lanv_canvas_set_need_repick(item->impl->canvas);
}

/**
 * lanv_item_show:
 * @item: A canvas item.
 *
 * Shows a canvas item.  If the item was already shown, then no action is taken.
 **/
void
lanv_item_show(LanvItem* item)
{
	g_return_if_fail(LANV_IS_ITEM(item));

	if (!(item->object.flags & LANV_ITEM_VISIBLE)) {
		item->object.flags |= LANV_ITEM_VISIBLE;
		lanv_canvas_request_redraw_w(item->impl->canvas,
		                             item->impl->x1, item->impl->y1,
		                             item->impl->x2 + 1, item->impl->y2 + 1);
		lanv_canvas_set_need_repick(item->impl->canvas);
	}
}

/**
 * lanv_item_hide:
 * @item: A canvas item.
 *
 * Hides a canvas item.  If the item was already hidden, then no action is
 * taken.
 **/
void
lanv_item_hide(LanvItem* item)
{
	g_return_if_fail(LANV_IS_ITEM(item));

	if (item->object.flags & LANV_ITEM_VISIBLE) {
		item->object.flags &= ~LANV_ITEM_VISIBLE;
		lanv_canvas_request_redraw_w(item->impl->canvas,
		                             item->impl->x1, item->impl->y1,
		                             item->impl->x2 + 1, item->impl->y2 + 1);
		lanv_canvas_set_need_repick(item->impl->canvas);
	}
}

#endif

void
lanv_item_i2w_offset(LanvItem* item, double* px, double* py)
{
	double x = 0.0;
	double y = 0.0;
#if 0
	while (item) {
		x += item->impl->x;
		y += item->impl->y;
		item = item->impl->parent;
	}
#endif
	*px = x;
	*py = y;
}

/**
 * lanv_item_i2w:
 * @item: A canvas item.
 * @x: X coordinate to convert (input/output value).
 * @y: Y coordinate to convert (input/output value).
 *
 * Converts a coordinate pair from item-relative coordinates to world
 * coordinates.
 **/
void
lanv_item_i2w(LanvItem* item, double* x, double* y)
{
	/*g_return_if_fail(LANV_IS_ITEM(item));
	  g_return_if_fail(x != NULL);
	  g_return_if_fail(y != NULL);*/

	double off_x = 0.0;
	double off_y = 0.0;
	lanv_item_i2w_offset(item, &off_x, &off_y);

	*x += off_x;
	*y += off_y;
}

void
lanv_item_i2w_pair(LanvItem* item, double* x1, double* y1, double* x2, double* y2)
{
	double off_x = 0.0;
	double off_y = 0.0;
	lanv_item_i2w_offset(item, &off_x, &off_y);

	*x1 += off_x;
	*y1 += off_y;
	*x2 += off_x;
	*y2 += off_y;
}

#if 0

/**
 * lanv_item_w2i:
 * @item: A canvas item.
 * @x: X coordinate to convert (input/output value).
 * @y: Y coordinate to convert (input/output value).
 *
 * Converts a coordinate pair from world coordinates to item-relative
 * coordinates.
 **/
void
lanv_item_w2i(LanvItem* item, double* x, double* y)
{
	double off_x = 0.0;
	double off_y = 0.0;
	lanv_item_i2w_offset(item, &off_x, &off_y);

	*x -= off_x;
	*y -= off_y;
}

/**
 * lanv_item_grab_focus:
 * @item: A canvas item.
 *
 * Makes the specified item take the keyboard focus, so all keyboard events will
 * be sent to it.  If the canvas widget itself did not have the focus, it grabs
 * it as well.
 **/
void
lanv_item_grab_focus(LanvItem* item)
{
	lanv_canvas_grab_focus(item->impl->canvas, item);
}

void
lanv_item_emit_event(LanvItem* item, GdkEvent* event, gint* finished)
{
	g_signal_emit(item, item_signals[ITEM_EVENT], 0, event, finished);
}

static void
lanv_item_default_bounds(LanvItem* item, double* x1, double* y1, double* x2, double* y2)
{
	(void)item;

	*x1 = *y1 = *x2 = *y2 = 0.0;
}

/**
 * lanv_item_get_bounds:
 * @item: A canvas item.
 * @x1: Leftmost edge of the bounding box (return value).
 * @y1: Upper edge of the bounding box (return value).
 * @x2: Rightmost edge of the bounding box (return value).
 * @y2: Lower edge of the bounding box (return value).
 *
 * Queries the bounding box of a canvas item.  The bounding box may not be
 * exactly tight, but the canvas items will do the best they can.  The bounds
 * are returned in the coordinate system of the item's parent.
 **/
void
lanv_item_get_bounds(LanvItem* item, double* x1, double* y1, double* x2, double* y2)
{
	LANV_ITEM_GET_CLASS(item)->bounds(item, x1, y1, x2, y2);
}

/**
 * lanv_item_request_update:
 * @item: A canvas item.
 *
 * To be used only by item implementations.  Requests that the canvas queue an
 * update for the specified item.
 **/
void
lanv_item_request_update(LanvItem* item)
{
	if (!item->impl->canvas) {
		/* Item is being / has been destroyed, ignore */
		return;
	}

	item->object.flags |= LANV_ITEM_NEED_UPDATE;

	if (item->impl->parent != NULL &&
	    !(item->impl->parent->object.flags & LANV_ITEM_NEED_UPDATE)) {
		/* Recurse up the tree */
		lanv_item_request_update(item->impl->parent);
	} else {
		/* Have reached the top of the tree, make sure the update call gets scheduled. */
		lanv_canvas_request_update(item->impl->canvas);
	}
}

void
lanv_item_set_wrapper(LanvItem* item, void* wrapper)
{
	item->impl->wrapper = wrapper;
}

void*
lanv_item_get_wrapper(LanvItem* item)
{
	return item->impl->wrapper;
}

static gboolean
boolean_handled_accumulator(GSignalInvocationHint* ihint,
                            GValue*                return_accu,
                            const GValue*          handler_return,
                            gpointer               dummy)
{
	(void)ihint;
	(void)dummy;

	gboolean continue_emission = FALSE;
	gboolean signal_handled    = FALSE;

	signal_handled = g_value_get_boolean(handler_return);
	g_value_set_boolean(return_accu, signal_handled);
	continue_emission = !signal_handled;

	return continue_emission;
}

/* Class initialization function for LanvItemClass */
static void
lanv_item_class_init(LanvItemClass* klass)
{
	GObjectClass* gobject_class = (GObjectClass*)klass;

	item_parent_class = (GtkObjectClass*)g_type_class_peek_parent(klass);

	gobject_class->set_property = lanv_item_set_property;
	gobject_class->get_property = lanv_item_get_property;

	g_object_class_install_property
		(gobject_class, ITEM_PROP_PARENT,
		 g_param_spec_object("parent", NULL, NULL,
		                     LANV_TYPE_ITEM,
		                     (GParamFlags)(G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property
		(gobject_class, ITEM_PROP_X,
		 g_param_spec_double("x",
		                     _("X"),
		                     _("X"),
		                     -G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
		                     (GParamFlags)(G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property
		(gobject_class, ITEM_PROP_Y,
		 g_param_spec_double("y",
		                     _("Y"),
		                     _("Y"),
		                     -G_MAXDOUBLE, G_MAXDOUBLE, 0.0,
		                     (GParamFlags)(G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property
		(gobject_class, ITEM_PROP_MANAGED,
		 g_param_spec_boolean("managed",
		                      _("Managed"),
		                      _("Whether the item is managed by its parent"),
		                      0,
		                      (GParamFlags)(G_PARAM_READABLE | G_PARAM_WRITABLE)));

#if LANV_GIR_ENABLED		// FIXME
	item_signals[ITEM_EVENT]
		= g_signal_new("event",
		               G_TYPE_FROM_CLASS(klass),
		               G_SIGNAL_RUN_LAST,
		               G_STRUCT_OFFSET(LanvItemClass, event),
		               boolean_handled_accumulator, NULL,
		               lanv_marshal_BOOLEAN__BOXED,
		               G_TYPE_BOOLEAN, 1,
		               GDK_TYPE_EVENT | G_SIGNAL_TYPE_STATIC_SCOPE);
#endif

	gobject_class->dispose = lanv_item_dispose;

	klass->realize   = lanv_item_realize;
	klass->unrealize = lanv_item_unrealize;
	klass->map       = lanv_item_map;
	klass->unmap     = lanv_item_unmap;
	klass->update    = lanv_item_update;
	klass->point     = lanv_item_point;
	klass->bounds    = lanv_item_default_bounds;
}
#endif
