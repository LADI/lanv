/* This file is part of Lanv.
 * Copyright 2007-2016 David Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2025 Nedko Arnaudov
 *
 * Lanv is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Lanv is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Lanv.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lanv-private.h"

#include <lanv/box.h>
#include <lanv/canvas.h>
#include <lanv/item.h>
#include <lanv/module.h>
#include <lanv/node.h>
#include <lanv/port.h>
#include <lanv/text.h>
#include <lanv/types.h>
#include <lanv/widget.h>

#include <cairo.h>

#if 0

#include <gdk/gdk.h>
#include <glib-object.h>
#include <glib.h>
#include <gobject/gclosure.h>
#include <gtk/gtk.h>

#endif

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FOREACH_PORT(ports, i) \
	for (LanvPort** i = (LanvPort**)ports->pdata; \
	     i != (LanvPort**)ports->pdata + ports->len; ++i)

#define FOREACH_PORT_CONST(ports, i) \
	for (const LanvPort** i = (const LanvPort**)ports->pdata; \
	     i != (const LanvPort**)ports->pdata + ports->len; ++i)

#if 0

static const double PAD              = 2.0;
static const double EDGE_PAD         = 5.0;
static const double MODULE_LABEL_PAD = 2.0;

G_DEFINE_TYPE_WITH_CODE(LanvModule, lanv_module, LANV_TYPE_BOX,
                        G_ADD_PRIVATE(LanvModule))

static LanvBoxClass* parent_class;

enum {
	PROP_0
};

static void
lanv_module_init(LanvModule* module)
{
	LanvModulePrivate* impl =
	    (LanvModulePrivate*)lanv_module_get_instance_private(module);

	module->impl = impl;

	LANV_NODE(module)->impl->can_head = FALSE;
	LANV_NODE(module)->impl->can_tail = FALSE;

	impl->ports = g_ptr_array_new();
	impl->embed_item    = NULL;
	impl->embed_width   = 0;
	impl->embed_height  = 0;
	impl->widest_input  = 0.0;
	impl->widest_output = 0.0;
	impl->must_reorder  = FALSE;
}

static void
lanv_module_destroy(GtkObject* object)
{
	g_return_if_fail(object != NULL);
	g_return_if_fail(LANV_IS_MODULE(object));

	LanvModule*        module = LANV_MODULE(object);
	LanvModulePrivate* impl   = module->impl;

	if (impl->ports) {
		FOREACH_PORT(impl->ports, p) {
			g_object_unref(GTK_OBJECT(*p));
		}
		g_ptr_array_free(impl->ports, TRUE);
		impl->ports = NULL;
	}

	if (impl->embed_item) {
		g_object_unref(GTK_OBJECT(impl->embed_item));
		impl->embed_item = NULL;
	}

	if (GTK_OBJECT_CLASS(parent_class)->destroy) {
		(*GTK_OBJECT_CLASS(parent_class)->destroy)(object);
	}
}

static void
lanv_module_set_property(GObject*      object,
                         guint         prop_id,
                         const GValue* value,
                         GParamSpec*   pspec)
{
	(void)value;

	g_return_if_fail(object != NULL);
	g_return_if_fail(LANV_IS_MODULE(object));

	switch (prop_id) {
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

static void
lanv_module_get_property(GObject*    object,
                         guint       prop_id,
                         GValue*     value,
                         GParamSpec* pspec)
{
	(void)value;

	g_return_if_fail(object != NULL);
	g_return_if_fail(LANV_IS_MODULE(object));

	switch (prop_id) {
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
		break;
	}
}

typedef struct {
	double   embed_x;
	double   width;
	double   input_width;
	double   output_width;
	gboolean horiz;
	gboolean embed_between;
} Metrics;

static void
title_size(LanvModule* module, double* w, double* h)
{
	if (module->box.node.impl->label) {
		g_object_get(G_OBJECT(module->box.node.impl->label),
		             "width", w,
		             "height", h,
		             NULL);
	} else {
		*w = *h = 0.0;
	}
}

static void
measure(LanvModule* module, Metrics* m)
{
	memset(m, '\0', sizeof(Metrics));

	double title_w = 0.0;
	double title_h = 0.0;
	title_size(module, &title_w, &title_h);

	LanvCanvas*        canvas       = lanv_item_get_canvas(LANV_ITEM(module));
	LanvText*          canvas_title = LANV_NODE(module)->impl->label;
	LanvModulePrivate* impl         = module->impl;

	if (lanv_canvas_get_direction(canvas) == LANV_DIRECTION_DOWN) {
		double contents_width = 0.0;
		if (canvas_title) {
			contents_width += title_w + (2.0 * PAD);
		}

		m->embed_x      = 0;
		m->input_width  = lanv_module_get_empty_port_breadth(module);
		m->output_width = lanv_module_get_empty_port_breadth(module);

		// TODO: cache this or merge with resize_right
		unsigned n_inputs  = 0;
		unsigned n_outputs = 0;
		FOREACH_PORT(impl->ports, pi) {
			if ((*pi)->impl->is_input) {
				++n_inputs;
			} else {
				++n_outputs;
			}
		}

		const unsigned hor_ports = MAX(1, MAX(n_inputs, n_outputs));
		const double ports_width = (2 * EDGE_PAD) +
			((m->input_width) * hor_ports) +
			((PAD + 1.0) * (hor_ports - 1));

		m->width = MAX(contents_width, ports_width);
		m->width = MAX(m->width, impl->embed_width);

		if (impl->embed_item) {
			m->width   = MAX(impl->embed_width + 2.0 * PAD, m->width);
			m->embed_x = PAD;
		}
		return;
	}

	// The amount of space between a port edge and the module edge (on the
	// side that the port isn't right on the edge).
	const double hor_pad = (canvas_title ? 10.0 : 20.0);

	m->width = (canvas_title) ? title_w + 10.0 : 1.0;

	// Title is wide or there is an embedded widget,
	// put inputs and outputs beside each other
	m->horiz = (impl->embed_item ||
	            (impl->widest_input + impl->widest_output + 10.0
	             < MAX(m->width, impl->embed_width)));

	// Fit ports to module (or vice-versa)
	m->input_width  = impl->widest_input;
	m->output_width = impl->widest_output;
	double expand_w = (m->horiz ? (m->width / 2.0) : m->width) - hor_pad;
	if (!impl->embed_item) {
		m->input_width  = MAX(impl->widest_input,  expand_w);
		m->output_width = MAX(impl->widest_output, expand_w);
	}

	const double widest = MAX(m->input_width, m->output_width);

	if (impl->embed_item) {
		double above_w   = MAX(m->width, widest + hor_pad);
		double between_w = MAX(m->width,
		                       (m->input_width
		                        + m->output_width
		                        + impl->embed_width));

		above_w = MAX(above_w, impl->embed_width);

		// Decide where to place embedded widget if necessary)
		if (impl->embed_width < impl->embed_height * 2.0) {
			m->embed_between = TRUE;
			m->width         = between_w;
			m->embed_x       = m->input_width;
		} else {
			m->width   = above_w;
			m->embed_x = 2.0;
		}
	}

	if (!canvas_title && (impl->widest_input == 0.0
	                      || impl->widest_output == 0.0)) {
		m->width += 10.0;
	}

	m->width += 4.0;
	m->width = MAX(m->width, widest + hor_pad);
}

static void
place_title(LanvModule* module, LanvDirection dir)
{
	LanvBox*  box          = LANV_BOX(module);
	LanvText* canvas_title = LANV_NODE(module)->impl->label;

	double title_w = 0.0;
	double title_h = 0.0;
	title_size(module, &title_w, &title_h);

	if (!canvas_title) {
		return;
	}

	LanvItem* t = LANV_ITEM(canvas_title);
	if (dir == LANV_DIRECTION_RIGHT) {
		t->impl->x = (lanv_box_get_width(box) - title_w) / 2.0;
		t->impl->y = 1.0;
	} else {
		t->impl->x = (lanv_box_get_width(box) - title_w) / 2.0;
		t->impl->y = lanv_module_get_empty_port_depth(module) + 1.0;
	}
}

static void
resize_right(LanvModule* module)
{
	LanvCanvas*        canvas = lanv_item_get_canvas(LANV_ITEM(module));
	LanvModulePrivate* impl   = module->impl;

	Metrics m;
	measure(module, &m);

	double title_w = 0.0;
	double title_h = 0.0;
	title_size(module, &title_w, &title_h);

	// Basic height contains title
	double header_height = title_h ? (3.0 + title_h) : EDGE_PAD;

	if (impl->embed_item) {
		lanv_item_set(impl->embed_item,
		              "x", (double)m.embed_x,
		              "y", header_height,
		              NULL);
	}

	// Actually set width and height
	lanv_box_set_width(LANV_BOX(module), m.width);

	// Offset ports below embedded widget
	if (!m.embed_between) {
		header_height += impl->embed_height;
	}

	// Move ports to appropriate locations
	double   in_y           = header_height;
	double   out_y          = header_height;
	FOREACH_PORT(impl->ports, pi) {
		LanvPort* const p     = (*pi);
		LanvBox*  const pbox  = LANV_BOX(p);
		LanvNode* const pnode = LANV_NODE(p);
		const double    h     = lanv_box_get_height(pbox);

		// Offset to shift ports to make borders line up
		const double border_off = (LANV_NODE(module)->impl->border_width -
		                           pnode->impl->border_width) / 2.0;

		if (p->impl->is_input) {
			lanv_node_move_to(pnode, -border_off, in_y + 1.0);
			lanv_box_set_width(pbox, m.input_width);
			in_y += h + pnode->impl->border_width + 1.0;

			lanv_canvas_for_each_edge_to(
				canvas, pnode,
				(LanvEdgeFunc)lanv_edge_update_location, NULL);
		} else {
			lanv_node_move_to(pnode, m.width - m.output_width + border_off, out_y + 1.0);
			lanv_box_set_width(pbox, m.output_width);
			out_y += h + pnode->impl->border_width + 1.0;

			lanv_canvas_for_each_edge_from(
				canvas, pnode,
				(LanvEdgeFunc)lanv_edge_update_location, NULL);
		}

		if (!m.horiz) {
			in_y  = MAX(in_y, out_y);
			out_y = MAX(in_y, out_y);
		}
	}

	double height = MAX(in_y, out_y) + EDGE_PAD;
	if (impl->embed_item && m.embed_between)
		height = MAX(height, impl->embed_height + header_height + 2.0);

	lanv_box_set_height(LANV_BOX(module), height);

	place_title(module, LANV_DIRECTION_RIGHT);
}

static void
resize_down(LanvModule* module)
{
	LanvCanvas*        canvas = lanv_item_get_canvas(LANV_ITEM(module));
	LanvModulePrivate* impl   = module->impl;

	Metrics m;
	measure(module, &m);

	double title_w = 0.0;
	double title_h = 0.0;
	title_size(module, &title_w, &title_h);

	const double port_depth   = lanv_module_get_empty_port_depth(module);
	const double port_breadth = lanv_module_get_empty_port_breadth(module);

	if (impl->embed_item) {
		lanv_item_set(impl->embed_item,
		              "x", (double)m.embed_x,
		              "y", port_depth + title_h,
		              NULL);
	}

	const double height = PAD + title_h
		+ impl->embed_height + (port_depth * 2.0);

	// Move ports to appropriate locations
	guint  in_count  = 0;
	guint  out_count = 0;
	double in_x      = 0.0;
	double out_x     = 0.0;
	FOREACH_PORT(impl->ports, pi) {
		LanvPort* const p     = (*pi);
		LanvBox*  const pbox  = LANV_BOX(p);
		LanvNode* const pnode = LANV_NODE(p);
		lanv_box_set_width(pbox, port_breadth);
		lanv_box_set_height(pbox, port_depth);

		// Offset to shift ports to make borders line up
		const double border_off = (LANV_NODE(module)->impl->border_width -
		                           pnode->impl->border_width) / 2.0;

		if (p->impl->is_input) {
			in_x = EDGE_PAD + (in_count++ * (port_breadth + PAD + 1.0));
			lanv_node_move_to(pnode, in_x, -border_off);
			lanv_canvas_for_each_edge_to(
				canvas, pnode,
				(LanvEdgeFunc)lanv_edge_update_location, NULL);
		} else {
			out_x = EDGE_PAD + (out_count++ * (port_breadth + PAD + 1.0));
			lanv_node_move_to(pnode, out_x, height - port_depth + border_off);
			lanv_canvas_for_each_edge_from(
				canvas, pnode,
				(LanvEdgeFunc)lanv_edge_update_location, NULL);
		}
	}

	lanv_box_set_height(LANV_BOX(module), height);
	lanv_box_set_width(LANV_BOX(module), m.width);
	place_title(module, LANV_DIRECTION_DOWN);
}

static void
measure_ports(LanvModule* module)
{
	LanvModulePrivate* impl = module->impl;

	impl->widest_input  = 0.0;
	impl->widest_output = 0.0;
	FOREACH_PORT_CONST(impl->ports, pi) {
		const LanvPort* const p = (*pi);
		const double          w = lanv_port_get_natural_width(p);
		if (p->impl->is_input) {
			if (w > impl->widest_input) {
				impl->widest_input = w;
			}
		} else {
			if (w > impl->widest_output) {
				impl->widest_output = w;
			}
		}
	}
}

static void
lanv_module_resize(LanvNode* self)
{
	LanvModule* module = LANV_MODULE(self);
	LanvNode*   node   = LANV_NODE(self);
	LanvCanvas* canvas = lanv_item_get_canvas(LANV_ITEM(module));

	double label_w = 0.0;
	double label_h = 0.0;
	if (node->impl->label) {
		g_object_get(node->impl->label,
		             "width", &label_w,
		             "height", &label_h,
		             NULL);
	}

	measure_ports(module);

	lanv_box_set_width(LANV_BOX(module), label_w + (MODULE_LABEL_PAD * 2.0));
	lanv_box_set_height(LANV_BOX(module), label_h);

	switch (lanv_canvas_get_direction(canvas)) {
	case LANV_DIRECTION_RIGHT:
		resize_right(module);
		break;
	case LANV_DIRECTION_DOWN:
		resize_down(module);
		break;
	}

	if (LANV_NODE_CLASS(parent_class)->resize) {
		LANV_NODE_CLASS(parent_class)->resize(self);
	}
}

static void
lanv_module_redraw_text(LanvNode* self)
{
	FOREACH_PORT(LANV_MODULE(self)->impl->ports, p) {
		lanv_node_redraw_text(LANV_NODE(*p));
	}

	if (parent_class->parent_class.redraw_text) {
		parent_class->parent_class.redraw_text(self);
	}
}

static void
lanv_module_add_port(LanvModule* module,
                     LanvPort*   port)
{
	LanvModulePrivate* impl = module->impl;

	// Update widest input/output measurements if necessary
	const double width = lanv_port_get_natural_width(port);
	if (port->impl->is_input && width > impl->widest_input) {
		impl->widest_input = width;
	} else if (!port->impl->is_input && width > impl->widest_output) {
		impl->widest_output = width;
	}

	// Add to port array
	g_ptr_array_add(impl->ports, port);

	// Request update with resize and reorder
	LANV_NODE(module)->impl->must_resize = TRUE;
	impl->must_reorder                   = TRUE;
}

static void
lanv_module_remove_port(LanvModule* module,
                        LanvPort*   port)
{
	gboolean removed = g_ptr_array_remove(module->impl->ports, port);
	if (removed) {
		const double width = lanv_box_get_width(LANV_BOX(port));
		// Find new widest input or output, if necessary
		if (port->impl->is_input && width >= module->impl->widest_input) {
			module->impl->widest_input = 0;
			FOREACH_PORT_CONST(module->impl->ports, i) {
				const LanvPort* const p = (*i);
				const double          w = lanv_box_get_width(LANV_BOX(p));
				if (p->impl->is_input && w >= module->impl->widest_input) {
					module->impl->widest_input = w;
				}
			}
		} else if (!port->impl->is_input && width >= module->impl->widest_output) {
			module->impl->widest_output = 0;
			FOREACH_PORT_CONST(module->impl->ports, i) {
				const LanvPort* const p = (*i);
				const double          w = lanv_box_get_width(LANV_BOX(p));
				if (!p->impl->is_input && w >= module->impl->widest_output) {
					module->impl->widest_output = w;
				}
			}
		}

		LANV_NODE(module)->impl->must_resize = TRUE;
	} else {
		fprintf(stderr, "Failed to find port to remove\n");
	}
}

static void
lanv_module_add(LanvItem* item, LanvItem* child)
{
	if (LANV_IS_PORT(child)) {
		lanv_module_add_port(LANV_MODULE(item), LANV_PORT(child));
	}
	lanv_item_request_update(item);
	if (LANV_ITEM_CLASS(parent_class)->add) {
		LANV_ITEM_CLASS(parent_class)->add(item, child);
	}
}

static void
lanv_module_remove(LanvItem* item, LanvItem* child)
{
	if (LANV_IS_PORT(child)) {
		lanv_module_remove_port(LANV_MODULE(item), LANV_PORT(child));
	}
	lanv_item_request_update(item);
	if (LANV_ITEM_CLASS(parent_class)->remove) {
		LANV_ITEM_CLASS(parent_class)->remove(item, child);
	}
}

static int
ptr_sort(const LanvPort** a, const LanvPort** b, const PortOrderCtx* ctx)
{
	return ctx->port_cmp(*a, *b, ctx->data);
}

static void
lanv_module_update(LanvItem* item, int flags)
{
	LanvModule* module = LANV_MODULE(item);
	LanvCanvas* canvas = lanv_item_get_canvas(item);

	if (module->impl->must_reorder) {
		// Sort ports array
		PortOrderCtx ctx = lanv_canvas_get_port_order(canvas);
		if (ctx.port_cmp) {
			g_ptr_array_sort_with_data(module->impl->ports,
			                           (GCompareDataFunc)ptr_sort,

			                           &ctx);
		}
		module->impl->must_reorder = FALSE;
	}

	if (module->impl->embed_item) {
		// Kick the embedded item to update position if we have moved
		lanv_item_move(LANV_ITEM(module->impl->embed_item), 0.0, 0.0);
	}

	FOREACH_PORT(module->impl->ports, p) {
		lanv_item_invoke_update(LANV_ITEM(*p), flags);
	}

	if (module->impl->embed_item) {
		lanv_item_invoke_update(LANV_ITEM(module->impl->embed_item), flags);
	}

	LANV_ITEM_CLASS(parent_class)->update(item, flags);
}

static void
lanv_module_draw(LanvItem* item,
                 cairo_t* cr, double cx, double cy, double cw, double ch)
{
	LanvNode*   node   = LANV_NODE(item);
	LanvModule* module = LANV_MODULE(item);

	// Draw box
	if (LANV_ITEM_CLASS(parent_class)->draw) {
		(*LANV_ITEM_CLASS(parent_class)->draw)(item, cr, cx, cy, cw, ch);
	}

	// Draw label
	if (node->impl->label) {
		LanvItem* label_item = LANV_ITEM(node->impl->label);
		LANV_ITEM_GET_CLASS(label_item)->draw(label_item, cr, cx, cy, cw, ch);
	}

	// Draw ports
	FOREACH_PORT(module->impl->ports, p) {
		LANV_ITEM_GET_CLASS(LANV_ITEM(*p))->draw(
			LANV_ITEM(*p), cr, cx, cy, cw, ch);
	}

	// Draw embed item
	if (module->impl->embed_item) {
		LANV_ITEM_GET_CLASS(module->impl->embed_item)->draw(
			module->impl->embed_item, cr, cx, cy, cw, ch);
	}
}

static void
lanv_module_move_to(LanvNode* node,
                    double    x,
                    double    y)
{
	LanvModule* module = LANV_MODULE(node);
	LANV_NODE_CLASS(parent_class)->move_to(node, x, y);
	FOREACH_PORT(module->impl->ports, p) {
		lanv_node_move(LANV_NODE(*p), 0.0, 0.0);
	}
	if (module->impl->embed_item) {
		lanv_item_move(LANV_ITEM(module->impl->embed_item), 0.0, 0.0);
	}
}

static void
lanv_module_move(LanvNode* node,
                 double    dx,
                 double    dy)
{
	LanvModule* module = LANV_MODULE(node);
	LANV_NODE_CLASS(parent_class)->move(node, dx, dy);
	FOREACH_PORT(module->impl->ports, p) {
		lanv_node_move(LANV_NODE(*p), 0.0, 0.0);
	}
	if (module->impl->embed_item) {
		lanv_item_move(LANV_ITEM(module->impl->embed_item), 0.0, 0.0);
	}
}

static double
lanv_module_point(LanvItem* item, double x, double y, LanvItem** actual_item)
{
	LanvModule* module = LANV_MODULE(item);

	double d = LANV_ITEM_CLASS(parent_class)->point(item, x, y, actual_item);

	if (!*actual_item) {
		// Point is not inside module at all, no point in checking children
		return d;
	}

	FOREACH_PORT(module->impl->ports, p) {
		LanvItem* const port = LANV_ITEM(*p);

		*actual_item = NULL;
		d = LANV_ITEM_GET_CLASS(port)->point(
			port, x - port->impl->x, y - port->impl->y, actual_item);

		if (*actual_item) {
			// Point is inside a port
			return d;
		}
	}

	// Point is inside module, but not a child port
	*actual_item = item;
	return 0.0;
}

static void
lanv_module_class_init(LanvModuleClass* klass)
{
	GObjectClass*   gobject_class = (GObjectClass*)klass;
	GtkObjectClass* object_class  = (GtkObjectClass*)klass;
	LanvItemClass*  item_class    = (LanvItemClass*)klass;
	LanvNodeClass*  node_class    = (LanvNodeClass*)klass;

	parent_class = LANV_BOX_CLASS(g_type_class_peek_parent(klass));

	gobject_class->set_property = lanv_module_set_property;
	gobject_class->get_property = lanv_module_get_property;

	object_class->destroy = lanv_module_destroy;

	item_class->add    = lanv_module_add;
	item_class->remove = lanv_module_remove;
	item_class->update = lanv_module_update;
	item_class->draw   = lanv_module_draw;
	item_class->point  = lanv_module_point;

	node_class->move        = lanv_module_move;
	node_class->move_to     = lanv_module_move_to;
	node_class->resize      = lanv_module_resize;
	node_class->redraw_text = lanv_module_redraw_text;
}

LanvModule*
lanv_module_new(LanvCanvas* canvas,
                const char* first_property_name, ...)
{
	LanvModule* module = LANV_MODULE(
		g_object_new(lanv_module_get_type(), "canvas", canvas, NULL));

	va_list args;
	va_start(args, first_property_name);
	g_object_set_valist(G_OBJECT(module), first_property_name, args);
	va_end(args);

	return module;
}

guint
lanv_module_num_ports(const LanvModule* module)
{
	return module->impl->ports ? module->impl->ports->len : 0;
}

LanvPort*
lanv_module_get_port(LanvModule* module,
                     guint       index)
{
	return (LanvPort*)g_ptr_array_index(module->impl->ports, index);
}

double
lanv_module_get_empty_port_breadth(const LanvModule* module)
{
	return lanv_module_get_empty_port_depth(module) * 2.0;
}

double
lanv_module_get_empty_port_depth(const LanvModule* module)
{
	LanvCanvas* canvas = lanv_item_get_canvas(LANV_ITEM(module));

	return lanv_canvas_get_font_size(canvas) * 1.1;
}

static void
on_embed_size_request(GtkWidget*      widget,
                      GtkRequisition* r,
                      void*           user_data)
{
	LanvModule*        module = LANV_MODULE(user_data);
	LanvModulePrivate* impl   = module->impl;
	if (impl->embed_width == r->width && impl->embed_height == r->height) {
		return;
	}

	impl->embed_width                    = r->width;
	impl->embed_height                   = r->height;
	LANV_NODE(module)->impl->must_resize = TRUE;

	GtkAllocation allocation;
	allocation.width = r->width;
	allocation.height = r->width;

	gtk_widget_size_allocate(widget, &allocation);
	lanv_item_set(impl->embed_item,
	              "width", (double)r->width,
	              "height", (double)r->height,
	              NULL);
}

void
lanv_module_embed(LanvModule* module,
                  GtkWidget*  widget)
{
	LanvModulePrivate* impl = module->impl;
	if (!widget && !impl->embed_item) {
		return;
	}

	if (impl->embed_item) {
		// Free existing embedded widget
		gtk_object_destroy(GTK_OBJECT(impl->embed_item));
		impl->embed_item = NULL;
	}

	if (!widget) {
		// Removing an existing embedded widget
		impl->embed_width                    = 0;
		impl->embed_height                   = 0;
		LANV_NODE(module)->impl->must_resize = TRUE;
		lanv_item_request_update(LANV_ITEM(module));
		return;
	}

	double title_w = 0.0;
	double title_h = 0.0;
	title_size(module, &title_w, &title_h);

	impl->embed_item = lanv_item_new(
		LANV_ITEM(module),
		lanv_widget_get_type(),
		"x", 2.0,
		"y", 4.0 + title_h,
		"widget", widget,
		NULL);

	GtkRequisition r;
	gtk_widget_show_all(widget);
	gtk_widget_size_request(widget, &r);
	on_embed_size_request(widget, &r, module);
	lanv_item_show(impl->embed_item);

	g_signal_connect(widget, "size-request",
	                 G_CALLBACK(on_embed_size_request), module);

	LANV_NODE(module)->impl->must_resize = TRUE;
	lanv_item_request_update(LANV_ITEM(module));
}

void
lanv_module_set_direction(LanvModule*   module,
                          LanvDirection direction)
{
	FOREACH_PORT(module->impl->ports, p) {
		lanv_port_set_direction(*p, direction);
	}
	LANV_NODE(module)->impl->must_resize = TRUE;
	lanv_item_request_update(LANV_ITEM(module));
}

void
lanv_module_for_each_port(LanvModule*  module,
                          LanvPortFunc f,
                          void*        data)
{
	LanvModulePrivate* impl = module->impl;
	const int          len  = impl->ports->len;
	LanvPort**         copy = (LanvPort**)malloc(sizeof(LanvPort*) * len);
	memcpy(copy, impl->ports->pdata, sizeof(LanvPort*) * len);

	for (int i = 0; i < len; ++i) {
		f(copy[i], data);
	}

	free(copy);
}

#endif
