/* This file is part of Lanv.
 * Copyright 2007-2013 David Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2025 Nedko Arnaudov
 *
 * Lanv is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Lanv is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Lanv.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <lanv/lanv.h>
#include <lanv/item.h>

#include <gtk/gtk.h>

#include <glib-object.h>
#include <glib.h>
#include <gobject/gclosure.h>

#include <stdio.h>

static void
on_window_destroy(GtkWidget* widget, void* data)
{
	(void)widget;
	(void)data;

	gtk_main_quit();
}

static void
on_connect(LanvCanvas* canvas, LanvNode* tail, LanvNode* head, void* data)
{
	(void)data;

	lanv_edge_new(canvas, tail, head, "color", 0xFFFFFFFF, NULL);
}

static void
on_disconnect(LanvCanvas* canvas, LanvNode* tail, LanvNode* head, void* data)
{
	(void)data;

	lanv_canvas_remove_edge_between(canvas, tail, head);
}

static void
on_value_changed(LanvPort* port, double value, void* data)
{
	(void)data;

	fprintf(stderr, "Value changed: port %p = %lf\n", (void*)port, value);
}

int
main(int argc, char** argv)
{
	gtk_init(&argc, &argv);

	GtkWindow* win = GTK_WINDOW(gtk_window_new(GTK_WINDOW_TOPLEVEL));
	gtk_window_set_title(win, "Lanv Test");
	g_signal_connect(win, "destroy",
	                 G_CALLBACK(on_window_destroy), NULL);

	LanvCanvas* canvas = lanv_canvas_new(1024, 768);
	gtk_container_add(GTK_CONTAINER(win), GTK_WIDGET(canvas));

	LanvCircle* circle = lanv_circle_new(canvas,
	                                     "x", 400.0,
	                                     "y", 400.0,
	                                     "draggable", TRUE,
	                                     "label", "state",
	                                     "radius", 32.0,
	                                     NULL);
	lanv_item_show(LANV_ITEM(circle));

	LanvModule* module = lanv_module_new(canvas,
	                                     "x", 10.0,
	                                     "y", 10.0,
	                                     "draggable", TRUE,
	                                     "label", "test",
	                                     NULL);

	lanv_port_new(module, FALSE,
	              "label", "Signal",
	              NULL);

	LanvPort* cport = lanv_port_new(module, TRUE,
	                                "label", "Control",
	                                NULL);
	lanv_port_show_control(cport);
	g_signal_connect(cport, "value-changed",
	                 G_CALLBACK(on_value_changed), NULL);

	//GtkWidget* entry = gtk_entry_new();
	//lanv_module_embed(module, entry);

	LanvPort* tport = lanv_port_new(module, TRUE,
	                                "label", "Toggle",
	                                NULL);
	lanv_port_show_control(tport);
	lanv_port_set_control_is_toggle(tport, TRUE);

	lanv_item_show(LANV_ITEM(module));

	LanvModule* module2 = lanv_module_new(canvas,
	                                      "x", 200.0,
	                                      "y", 10.0,
	                                      "draggable", TRUE,
	                                      "label", "test2",
	                                      NULL);

	lanv_port_new(module2, TRUE,
	              "label", "Signal",
	              NULL);

	g_signal_connect(canvas, "connect",
	                 G_CALLBACK(on_connect), canvas);

	g_signal_connect(canvas, "disconnect",
	                 G_CALLBACK(on_disconnect), canvas);

	lanv_item_show(LANV_ITEM(module2));

	gtk_widget_show_all(GTK_WIDGET(win));
	gtk_window_present(win);
	gtk_main();

	return 0;
}
