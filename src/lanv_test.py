#!/usr/bin/env python

from gi.repository import Lanv, Gtk

win = Gtk.Window()
win.set_title("Lanv Python Test")
win.connect("destroy", lambda obj: Gtk.main_quit())

canvas = Lanv.Canvas.new(1024, 768)
module = Lanv.Module(canvas=canvas,
                     label="Test")
# iport = Lanv.Port(module=module,
#                   is_input=True,
#                   label="In")
# oport = Lanv.Port(module=module,
#                   is_input=False,
#                   label="In")

win.add(canvas)
win.show_all()
win.present()
Gtk.main()
