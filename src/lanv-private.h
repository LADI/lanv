/* This file is part of Lanv.
 * Copyright 2007-2015 David Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2025 Nedko Arnaudov
 *
 * Lanv is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Lanv is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Lanv.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LANV_PRIVATE_H
#define LANV_PRIVATE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <lanv/canvas.h>
#include <lanv/item.h>
#include <lanv/text.h>
#include <lanv/types.h>

#include <cairo.h>

#if 0
#include <gdk/gdk.h>
#include <glib.h>
#include <gtk/gtk.h>
#endif
#include <pango/pango-layout.h>

#define LANV_CLOSE_ENOUGH 1

extern guint signal_moved;

/* Box */

typedef struct {
	double   x1, y1, x2, y2;
	double   border_width;
	gboolean stacked;
} LanvBoxCoords;

struct _LanvBoxPrivate {
	LanvBoxCoords coords;
	LanvBoxCoords old_coords;
	double        radius_tl;
	double        radius_tr;
	double        radius_br;
	double        radius_bl;
	gboolean      beveled;
};

/* Circle */

typedef struct {
	double x, y, radius, radius_ems;
	double width;
} LanvCircleCoords;

struct _LanvCirclePrivate {
	LanvCircleCoords coords;
	LanvCircleCoords old_coords;
	gboolean         fit_label;
};

/* Edge */

typedef struct {
	double   x1, y1, x2, y2;
	double   cx1, cy1, cx2, cy2;
	double   handle_x, handle_y, handle_radius;
	double   width;
	gboolean constraining;
	gboolean curved;
	gboolean arrowhead;
} LanvEdgeCoords;

struct _LanvEdgePrivate
{
	LanvNode*       tail;
	LanvNode*       head;
	LanvEdgeCoords  coords;
	LanvEdgeCoords  old_coords;
	double          dash_length;
	double          dash_offset;
	guint           color;
	gboolean        selected;
	gboolean        highlighted;
	gboolean        ghost;
};

/* Module */

#if 0

struct _LanvModulePrivate
{
	GPtrArray* ports;
	LanvItem*  embed_item;
	int        embed_width;
	int        embed_height;
	double     widest_input;
	double     widest_output;
	gboolean   must_reorder;
};

/* Node */

#ifdef LANV_FDGL
typedef struct {
	double x;
	double y;
} Vector;
#endif

struct _LanvNodePrivate {
	struct _LanvNode* partner;
	LanvText*         label;
	double            dash_length;
	double            dash_offset;
	double            border_width;
	guint             fill_color;
	guint             border_color;
	gboolean          can_tail;
	gboolean          can_head;
	gboolean          is_source;
	gboolean          selected;
	gboolean          highlighted;
	gboolean          draggable;
	gboolean          show_label;
	gboolean          grabbed;
	gboolean          must_resize;
#ifdef LANV_FDGL
	Vector            force;
	Vector            vel;
	gboolean          connected;
#endif
};

/* Widget */

struct _LanvWidgetPrivate {
	GtkWidget* widget;          /* The child widget */

	double        x, y;			/* Position at anchor */
	double        width, height; /* Dimensions of widget */
	GtkAnchorType anchor;		/* Anchor side for widget */

	int cx, cy;                 /* Top-left canvas coordinates for widget */
	int cwidth, cheight;		/* Size of widget in pixels */

	guint destroy_id;           /* Signal connection id for destruction of child widget */

	guint size_pixels : 1;		/* Is size specified in (unchanging) pixels or units (get scaled)? */
	guint in_destroy : 1;		/* Is child widget being destroyed? */
};

/* Group */
struct _LanvGroupPrivate {
	GList* item_list;
	GList* item_list_end;
};

/* Item */
struct _LanvItemPrivate {
	/* Parent canvas for this item */
	struct _LanvCanvas* canvas;

	/* Parent for this item */
	LanvItem* parent;

	/* Wrapper object for this item, if any */
	void* wrapper;

	/* Layer (z order), higher values are on top */
	guint layer;

	/* Position in parent-relative coordinates. */
	double x, y;

	/* Bounding box for this item (in world coordinates) */
	double x1, y1, x2, y2;

	/* True if parent manages this item (don't call add/remove) */
	gboolean managed;
};

void
lanv_node_tick(LanvNode* self, double seconds);

void
lanv_node_tail_vector(const LanvNode* self,
                      const LanvNode* head,
                      double*         x1,
                      double*         y1,
                      double*         x2,
                      double*         y2);

void
lanv_node_head_vector(const LanvNode* self,
                      const LanvNode* tail,
                      double*         x1,
                      double*         y1,
                      double*         x2,
                      double*         y2);

/**
 * lanv_node_get_draw_properties:
 *
 * Get the colours that should currently be used for drawing this node.  Note
 * these may not be identical to the property values because of highlighting
 * and selection.
 */
void
lanv_node_get_draw_properties(const LanvNode* node,
                              double*         dash_length,
                              double*         border_color,
                              double*         fill_color);

/* Port */

typedef struct {
	LanvBox*  rect;
	float     value;
	float     min;
	float     max;
	gboolean  is_toggle;
	gboolean  is_integer;
} LanvPortControl;

struct _LanvPortPrivate {
	LanvPortControl* control;
	LanvText*        value_label;
	gboolean         is_input;
	gboolean         is_controllable;
};

/* Text */

typedef struct
{
	double x;
	double y;
	double width;
	double height;
} LanvTextCoords;

struct _LanvTextPrivate
{
	PangoLayout*     layout;
	char*            text;
	LanvTextCoords   coords;
	LanvTextCoords   old_coords;
	double           font_size;
	guint            color;
	gboolean         needs_layout;
};

/* Canvas */

typedef struct {
	LanvPortOrderFunc port_cmp;
	void*             data;
} PortOrderCtx;

#endif

void
lanv_canvas_move_selected_items(LanvCanvas* canvas,
                                double      dx,
                                double      dy);

void
lanv_canvas_selection_move_finished(LanvCanvas* canvas);

void
lanv_canvas_add_node(LanvCanvas* canvas,
                     LanvNode*   node);

void
lanv_canvas_remove_node(LanvCanvas* canvas,
                        LanvNode*   node);

void
lanv_canvas_select_node(LanvCanvas* canvas,
                        LanvNode*   node);

void
lanv_canvas_unselect_node(LanvCanvas* canvas,
                          LanvNode*   node);

void
lanv_canvas_add_edge(LanvCanvas* canvas,
                     LanvEdge*   edge);

void
lanv_canvas_select_edge(LanvCanvas* canvas,
                        LanvEdge*   edge);

void
lanv_canvas_unselect_edge(LanvCanvas* canvas,
                          LanvEdge*   edge);

void
lanv_canvas_disconnect_edge(LanvCanvas* canvas,
                            LanvEdge*   edge);

#if 0
gboolean
lanv_canvas_port_event(LanvCanvas* canvas,
                       LanvPort*   port,
                       GdkEvent*   event);
#endif

void
lanv_canvas_contents_changed(LanvCanvas* canvas);

void
lanv_item_i2w_offset(LanvItem* item, double* px, double* py);

void
lanv_item_i2w_pair(LanvItem* item, double* x1, double* y1, double* x2, double* y2);

void
lanv_item_invoke_update(LanvItem* item, int flags);

#if 0
void
lanv_item_emit_event(LanvItem* item, GdkEvent* event, gint* finished);
#endif

void
lanv_canvas_request_update(LanvCanvas* canvas);

#if 0
int
lanv_canvas_emit_event(LanvCanvas* canvas, GdkEvent* event);
#endif

void
lanv_canvas_set_need_repick(LanvCanvas* canvas);

void
lanv_canvas_forget_item(LanvCanvas* canvas, LanvItem* item);

void
lanv_canvas_grab_focus(LanvCanvas* canvas, LanvItem* item);

void
lanv_canvas_get_zoom_offsets(LanvCanvas* canvas, int* x, int* y);

#if 0
int
lanv_canvas_grab_item(LanvItem* item, guint event_mask, GdkCursor* cursor, guint32 etime);
#endif

void
lanv_canvas_ungrab_item(LanvItem* item, guint32 etime);

/* Request a redraw of the specified rectangle in canvas coordinates */
void
lanv_canvas_request_redraw_c(LanvCanvas* canvas,
                             int x1, int y1, int x2, int y2);

/* Request a redraw of the specified rectangle in world coordinates */
void
lanv_canvas_request_redraw_w(LanvCanvas* canvas,
                             double x1, double y1, double x2, double y2);

#if 0
PortOrderCtx
lanv_canvas_get_port_order(LanvCanvas* canvas);
#endif

gboolean
lanv_canvas_exporting(LanvCanvas* canvas);

/* Edge */

void
lanv_edge_update_location(LanvEdge* edge);

void
lanv_edge_get_coords(const LanvEdge* edge, LanvEdgeCoords* coords);

void
lanv_edge_request_redraw(LanvItem*             item,
                         const LanvEdgeCoords* coords);

void
lanv_edge_tick(LanvEdge* edge, double seconds);

/* Box */

void
lanv_box_path(LanvBox* box,
              cairo_t* cr, double x1, double y1, double x2, double  y2,
              double dr);

void
lanv_box_request_redraw(LanvItem*            item,
                        const LanvBoxCoords* coords,
                        gboolean             world);

/* Port */

void
lanv_port_set_control_value_internal(LanvPort* port,
                                     float     value);

void
lanv_port_set_direction(LanvPort*     port,
                        LanvDirection direction);

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif  /* LANV_PRIVATE_H */
