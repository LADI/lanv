#!/bin/sh

DOC_MODULE=lanv

pwd=`pwd`
echo "gtkdoc.sh: Entering directory \`$pwd/docs'"

mkdir -p docs
cd docs

export CFLAGS="`pkg-config --cflags lanv-1`"
export LDFLAGS="`pkg-config --libs lanv-1`"

# Sources have changed

gtkdoc-scan --rebuild-sections --rebuild-types --ignore-headers=types.h --module=$DOC_MODULE --source-dir=../lanv
gtkdoc-scangobj --module=$DOC_MODULE
gtkdoc-mkdb --module=$DOC_MODULE --output-format=xml --source-dir=../lanv

# XML files have changed
mkdir -p html
cd html && gtkdoc-mkhtml $DOC_MODULE ../lanv-docs.xml && cd -
gtkdoc-fixxref --module=$DOC_MODULE --module-dir=html

echo "gtkdoc.sh: Leaving directory \`$pwd/docs'"
cd -
