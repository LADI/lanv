/* This file is part of Lanv.
 * Copyright 2007-2014 David Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2025 Nedko Arnaudov
 *
 * Lanv is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Lanv is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Lanv.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Based on GnomeCanvasWidget, by Federico Mena <federico@nuclecu.unam.mx>
 * Copyright 1997-2000 Free Software Foundation
 */

#ifndef LANV_WIDGET_H
#define LANV_WIDGET_H

#include <lanv/item.h>

#if 0

#include <glib-object.h>
#include <glib.h>

G_BEGIN_DECLS

#define LANV_TYPE_WIDGET            (lanv_widget_get_type ())
#define LANV_WIDGET(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), LANV_TYPE_WIDGET, LanvWidget))
#define LANV_WIDGET_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), LANV_TYPE_WIDGET, LanvWidgetClass))
#define LANV_IS_WIDGET(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LANV_TYPE_WIDGET))
#define LANV_IS_WIDGET_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LANV_TYPE_WIDGET))
#define LANV_WIDGET_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), LANV_TYPE_WIDGET, LanvWidgetClass))

struct _LanvWidget;
struct _LanvWidgetClass;

typedef struct _LanvWidget        LanvWidget;
typedef struct _LanvWidgetPrivate LanvWidgetPrivate;
typedef struct _LanvWidgetClass   LanvWidgetClass;

struct _LanvWidget {
	LanvItem           item;
	LanvWidgetPrivate* impl;
};

struct _LanvWidgetClass {
	LanvItemClass parent_class;

	/* Reserved for future expansion */
	gpointer spare_vmethods [4];
};

GType lanv_widget_get_type(void) G_GNUC_CONST;

G_END_DECLS

#endif

#endif  /* LANV_WIDGET_H */
