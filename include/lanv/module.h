/* This file is part of Lanv.
 * Copyright 2007-2014 David Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2025 Nedko Arnaudov
 *
 * Lanv is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Lanv is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Lanv.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LANV_MODULE_H
#define LANV_MODULE_H

#include <lanv/box.h>
#include <lanv/canvas.h>
#include <lanv/types.h>

#if 0
#include <glib-object.h>
#include <glib.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define LANV_TYPE_MODULE            (lanv_module_get_type())
#define LANV_MODULE(obj)            (GTK_CHECK_CAST((obj), LANV_TYPE_MODULE, LanvModule))
#define LANV_MODULE_CLASS(klass)    (GTK_CHECK_CLASS_CAST((klass), LANV_TYPE_MODULE, LanvModuleClass))
#define LANV_IS_MODULE(obj)         (GTK_CHECK_TYPE((obj), LANV_TYPE_MODULE))
#define LANV_IS_MODULE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), LANV_TYPE_MODULE))
#define LANV_MODULE_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS((obj), LANV_TYPE_MODULE, LanvModuleClass))

struct _LanvModuleClass;

typedef struct _LanvModuleClass   LanvModuleClass;
typedef struct _LanvModulePrivate LanvModulePrivate;

typedef void (*LanvPortFunc)(LanvPort* port, void* data);

struct _LanvModule {
	LanvBox            box;
	LanvModulePrivate* impl;
};

struct _LanvModuleClass {
	LanvBoxClass parent_class;

	/* Reserved for future expansion */
	gpointer spare_vmethods[4];
};

GType lanv_module_get_type(void) G_GNUC_CONST;

LanvModule*
lanv_module_new(LanvCanvas* canvas,
                const char* first_prop_name, ...);

guint
lanv_module_num_ports(const LanvModule* module);

/**
 * lanv_module_get_port:
 *
 * Get a port by index.
 *
 * Return value: (transfer none): The port on @module at @index.
 */
LanvPort*
lanv_module_get_port(LanvModule* module,
                     guint       index);

double
lanv_module_get_empty_port_breadth(const LanvModule* module);

double
lanv_module_get_empty_port_depth(const LanvModule* module);

void
lanv_module_embed(LanvModule* module, GtkWidget* widget);

void
lanv_module_set_direction(LanvModule* module, LanvDirection direction);

/**
 * lanv_module_for_each_port:
 * @module: The module.
 * @f: (scope call): A function to call on every port on @module.
 * @data: User data to pass to @f.
 */
void
lanv_module_for_each_port(LanvModule*  module,
                          LanvPortFunc f,
                          void*        data);

G_END_DECLS

#endif

#endif  /* LANV_MODULE_H */
