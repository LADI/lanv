/* This file is part of Lanv.
 * Copyright 2007-2015 David Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2025 Nedko Arnaudov
 *
 * Lanv is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Lanv is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Lanv.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LANV_EDGE_H
#define LANV_EDGE_H

#include <lanv/item.h>
#include <lanv/types.h>

#if 0
#include <glib-object.h>
#include <glib.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define LANV_TYPE_EDGE            (lanv_edge_get_type())
#define LANV_EDGE(obj)            (GTK_CHECK_CAST((obj), LANV_TYPE_EDGE, LanvEdge))
#define LANV_EDGE_CLASS(klass)    (GTK_CHECK_CLASS_CAST((klass), LANV_TYPE_EDGE, LanvEdgeClass))
#define LANV_IS_EDGE(obj)         (GTK_CHECK_TYPE((obj), LANV_TYPE_EDGE))
#define LANV_IS_EDGE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), LANV_TYPE_EDGE))
#define LANV_EDGE_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS((obj), LANV_TYPE_EDGE, LanvEdgeClass))
#endif

struct _LanvEdgeClass;

typedef struct _LanvEdgeClass   LanvEdgeClass;
typedef struct _LanvEdgePrivate LanvEdgePrivate;

struct _LanvEdge {
#if 0
	LanvItem         item;
	LanvEdgePrivate* impl;
#endif
};

#if 0
struct _LanvEdgeClass {
	LanvItemClass parent_class;

	/* Reserved for future expansion */
	gpointer spare_vmethods[4];
};

GType lanv_edge_get_type(void) G_GNUC_CONST;
#endif

LanvEdge*
lanv_edge_new(LanvCanvas* canvas,
              LanvNode*   tail,
              LanvNode*   head,
              const char* first_prop_name, ...);

gboolean
lanv_edge_is_within(const LanvEdge* edge,
                    double          x1,
                    double          y1,
                    double          x2,
                    double          y2);

gboolean
lanv_edge_get_curved(const LanvEdge* edge);

void
lanv_edge_set_curved(LanvEdge* edge, gboolean curved);

gboolean
lanv_edge_get_constraining(const LanvEdge* edge);

void
lanv_edge_set_constraining(LanvEdge* edge, gboolean constraining);

void
lanv_edge_set_selected(LanvEdge* edge, gboolean selected);

void
lanv_edge_set_highlighted(LanvEdge* edge, gboolean highlighted);

void
lanv_edge_select(LanvEdge* edge);

void
lanv_edge_unselect(LanvEdge* edge);

void
lanv_edge_highlight(LanvEdge* edge);

void
lanv_edge_unhighlight(LanvEdge* edge);

/**
 * lanv_edge_disconnect:
 *
 * Disconnect the edge.  This will disconnect the edge just as if it had been
 * disconnected by the user via the canvas.  The canvas disconnect signal will
 * be emitted, allowing the application to control disconnect logic.
 */
void
lanv_edge_disconnect(LanvEdge* edge);

/**
 * lanv_edge_remove:
 *
 * Remove the edge from the canvas.  This will only remove the edge visually,
 * it will not emit the canvas disconnect signal to notify the application.
 */
void
lanv_edge_remove(LanvEdge* edge);

/**
 * lanv_edge_get_tail:
 *
 * Return value: (transfer none): The tail of `edge`.
 */
LanvNode*
lanv_edge_get_tail(const LanvEdge* edge);

/**
 * lanv_edge_get_head:
 *
 * Return value: (transfer none): The head of `edge`.
 */
LanvNode*
lanv_edge_get_head(const LanvEdge* edge);

#if 0
G_END_DECLS
#endif

#endif  /* LANV_EDGE_H */
