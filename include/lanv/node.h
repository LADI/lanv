/* This file is part of Lanv.
 * Copyright 2007-2014 David Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2025 Nedko Arnaudov
 *
 * Lanv is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Lanv is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Lanv.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LANV_NODE_H
#define LANV_NODE_H

#include <lanv/item.h>
#include <lanv/types.h>

#if 0

#include <glib-object.h>
#include <glib.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define LANV_TYPE_NODE            (lanv_node_get_type())
#define LANV_NODE(obj)            (GTK_CHECK_CAST((obj), LANV_TYPE_NODE, LanvNode))
#define LANV_NODE_CLASS(klass)    (GTK_CHECK_CLASS_CAST((klass), LANV_TYPE_NODE, LanvNodeClass))
#define LANV_IS_NODE(obj)         (GTK_CHECK_TYPE((obj), LANV_TYPE_NODE))
#define LANV_IS_NODE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), LANV_TYPE_NODE))
#define LANV_NODE_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS((obj), LANV_TYPE_NODE, LanvNodeClass))

#endif

struct _LanvNodeClass;

typedef struct _LanvNodeClass   LanvNodeClass;
typedef struct _LanvNodePrivate LanvNodePrivate;

struct _LanvNode {
#if 0
	LanvItem         item;
	LanvNodePrivate* impl;
#endif
};

#if 0
struct _LanvNodeClass {
	LanvItemClass parent_class;

	void (*tick)(LanvNode* self,
	             double    seconds);

	void (*move)(LanvNode* node,
	             double    dx,
	             double    dy);

	void (*move_to)(LanvNode* node,
	                double    x,
	                double    y);

	void (*resize)(LanvNode* node);

	void (*redraw_text)(LanvNode* node);

	void (*disconnect)(LanvNode* node);

	gboolean (*is_within)(const LanvNode* self,
	                      double          x1,
	                      double          y1,
	                      double          x2,
	                      double          y2);

	void (*tail_vector)(const LanvNode* self,
	                    const LanvNode* head,
	                    double*         x,
	                    double*         y,
	                    double*         dx,
	                    double*         dy);

	void (*head_vector)(const LanvNode* self,
	                    const LanvNode* tail,
	                    double*         x,
	                    double*         y,
	                    double*         dx,
	                    double*         dy);

	/* Reserved for future expansion */
	gpointer spare_vmethods[4];
};

GType lanv_node_get_type(void) G_GNUC_CONST;

#endif

/**
 * lanv_node_can_tail:
 *
 * Return value: True iff node can act as the tail of an edge.
 */
gboolean
lanv_node_can_tail(const LanvNode* node);

/**
 * lanv_node_can_head:
 *
 * Return value: True iff node can act as the head of an edge.
 */
gboolean
lanv_node_can_head(const LanvNode* node);

/**
 * lanv_node_set_is_source:
 *
 * Flag a node as a source.  This information is used to influence layout.
 */
void
lanv_node_set_is_source(const LanvNode* node, gboolean is_source);

/**
 * lanv_node_is_within:
 *
 * Return value: True iff node is entirely within the given rectangle.
 */
gboolean
lanv_node_is_within(const LanvNode* node,
                    double          x1,
                    double          y1,
                    double          x2,
                    double          y2);

const char* lanv_node_get_label(const LanvNode* node);

double lanv_node_get_border_width(const LanvNode* node);

void lanv_node_set_border_width(const LanvNode* node, double border_width);

double lanv_node_get_dash_length(const LanvNode* node);

void lanv_node_set_dash_length(const LanvNode* node, double dash_length);

double lanv_node_get_dash_offset(const LanvNode* node);

void lanv_node_set_dash_offset(const LanvNode* node, double dash_offset);

guint lanv_node_get_fill_color(const LanvNode* node);

void lanv_node_set_fill_color(const LanvNode* node, guint fill_color);

guint lanv_node_get_border_color(const LanvNode* node);

void lanv_node_set_border_color(const LanvNode* node, guint border_color);

/**
 * lanv_node_get_partner:
 *
 * Return value: (transfer none): The partner of @node.
 */
LanvNode*
lanv_node_get_partner(const LanvNode* node);

void lanv_node_set_label(LanvNode* node, const char* str);
void lanv_node_set_show_label(LanvNode* node, gboolean show);

void
lanv_node_move(LanvNode* node,
               double    dx,
               double    dy);

void
lanv_node_move_to(LanvNode* node,
                  double    x,
                  double    y);

void
lanv_node_resize(LanvNode* node);

void
lanv_node_redraw_text(LanvNode* node);

void
lanv_node_disconnect(LanvNode* node);

gboolean
lanv_node_is_selected(LanvNode* node);

G_END_DECLS

#endif  /* LANV_NODE_H */
