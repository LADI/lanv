/* This file is part of Lanv.
 * Copyright 2007-2016 David Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2025 Nedko Arnaudov
 *
 * Lanv is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Lanv is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Lanv.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LANV_LANV_H
#define LANV_LANV_H

#include <lanv/box.h>    // IWYU pragma: export
#include <lanv/canvas.h> // IWYU pragma: export
#include <lanv/circle.h> // IWYU pragma: export
#include <lanv/edge.h>   // IWYU pragma: export
#include <lanv/group.h>  // IWYU pragma: export
#include <lanv/module.h> // IWYU pragma: export
#include <lanv/node.h>   // IWYU pragma: export
#include <lanv/port.h>   // IWYU pragma: export
#include <lanv/text.h>   // IWYU pragma: export
#include <lanv/types.h>  // IWYU pragma: export

#endif  /* LANV_LANV_H */
