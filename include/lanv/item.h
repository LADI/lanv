/* This file is part of Lanv.
 * Copyright 2007-2016 David Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2025 Nedko Arnaudov
 *
 * Lanv is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Lanv is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Lanv.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Based on GnomeCanvas, by Federico Mena <federico@nuclecu.unam.mx>
 * and Raph Levien <raph@gimp.org>
 * Copyright 1997-2000 Free Software Foundation
 */

#ifndef LANV_ITEM_H
#define LANV_ITEM_H

#include <cairo.h>
#if 0
#include <gdk/gdk.h>
#include <glib-object.h>
#include <glib.h>
#include <gtk/gtk.h>
#endif

#include <stdarg.h>

#if 0
G_BEGIN_DECLS
#endif

struct _LanvItem;
struct _LanvItemClass;

typedef struct _LanvItem        LanvItem;
typedef struct _LanvItemPrivate LanvItemPrivate;
typedef struct _LanvItemClass   LanvItemClass;

/* Object flags for items */
enum {
	LANV_ITEM_REALIZED      = 1 << 1,
	LANV_ITEM_MAPPED        = 1 << 2,
	LANV_ITEM_ALWAYS_REDRAW = 1 << 3,
	LANV_ITEM_VISIBLE       = 1 << 4,
	LANV_ITEM_NEED_UPDATE   = 1 << 5,
	LANV_ITEM_NEED_VIS      = 1 << 6
};

#if 0
#define LANV_TYPE_ITEM            (lanv_item_get_type())
#define LANV_ITEM(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj), LANV_TYPE_ITEM, LanvItem))
#define LANV_ITEM_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass), LANV_TYPE_ITEM, LanvItemClass))
#define LANV_IS_ITEM(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj), LANV_TYPE_ITEM))
#define LANV_IS_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), LANV_TYPE_ITEM))
#define LANV_ITEM_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), LANV_TYPE_ITEM, LanvItemClass))

struct _LanvItem {
	GtkObject        object;
	LanvItemPrivate* impl;
};
#endif

struct _LanvItemClass {
#if 0
	GtkObjectClass parent_class;
#endif

	/* Add a child to this item (optional). */
	void (*add)(LanvItem* item, LanvItem* child);

	/* Remove a child from this item (optional). */
	void (*remove)(LanvItem* item, LanvItem* child);

	/* Tell the item to update itself.
	 *
	 * The flags are from the update flags defined above.  The item should
	 * update its internal state from its queued state, and recompute and
	 * request its repaint area.  The update method also recomputes the
	 * bounding box of the item.
	 */
	void (*update)(LanvItem* item, int flags);

	/* Realize an item (create GCs, etc.). */
	void (*realize)(LanvItem* item);

	/* Unrealize an item. */
	void (*unrealize)(LanvItem* item);

	/* Map an item - normally only need by items with their own GdkWindows. */
	void (*map)(LanvItem* item);

	/* Unmap an item */
	void (*unmap)(LanvItem* item);

	/* Draw an item of this type.
	 *
	 * (cx, cy) and (width, height) describe the rectangle being drawn in
	 * world-relative coordinates.
	 */
	void (*draw)(LanvItem* item,
	             cairo_t*  cr,
	             double    cx,
	             double    cy,
	             double    cw,
	             double    ch);

	/* Calculate the distance from an item to the specified point.
	 *
	 * It also returns a canvas item which is actual item the point is within,
	 * which may not be equal to @item if @item has children.
	 * (x, y) are item-relative coordinates.
	 */
	double (*point)(LanvItem*  item,
	                double     x,
	                double     y,
	                LanvItem** actual_item);

	/* Fetch the item's bounding box (need not be exactly tight).
	 *
	 * This should be in item-relative coordinates.
	 */
	void (*bounds)(LanvItem* item, double* x1, double* y1, double* x2, double* y2);

#if 0
	/* Signal: an event occurred for an item of this type.
	 *
	 * The (x, y) coordinates are in the canvas world coordinate system.
	 */
	gboolean (*event)(LanvItem* item, GdkEvent* event);

	/* Reserved for future expansion */
	gpointer spare_vmethods[4];
#endif
};

#if 0
GType lanv_item_get_type(void) G_GNUC_CONST;

LanvItem* lanv_item_new(LanvItem* parent, GType type,
                        const gchar* first_arg_name, ...);

void lanv_item_construct(LanvItem* item, LanvItem* parent,
                         const gchar* first_arg_name, va_list args);

void lanv_item_set(LanvItem* item, const gchar* first_arg_name, ...);

void lanv_item_set_valist(LanvItem* item,
                          const gchar* first_arg_name, va_list args);
#endif

/**
 * lanv_item_get_canvas:
 * @item: The item.
 *
 * Return value: (transfer none): The canvas @item is on.
 */
struct _LanvCanvas* lanv_item_get_canvas(LanvItem* item);

/**
 * lanv_item_get_parent:
 * @item: The item.
 *
 * Return value: (transfer none): The parent of @item.
 */
LanvItem* lanv_item_get_parent(LanvItem* item);

void lanv_item_raise(LanvItem* item);

void lanv_item_lower(LanvItem* item);

void lanv_item_move(LanvItem* item, double dx, double dy);

void lanv_item_show(LanvItem* item);

void lanv_item_hide(LanvItem* item);

void lanv_item_i2w(LanvItem* item, double* x, double* y);

void lanv_item_w2i(LanvItem* item, double* x, double* y);

void lanv_item_grab_focus(LanvItem* item);

void lanv_item_get_bounds(LanvItem* item,
                          double* x1, double* y1, double* x2, double* y2);

void lanv_item_request_update(LanvItem* item);

void lanv_item_set_wrapper(LanvItem* item, void* wrapper);

void* lanv_item_get_wrapper(LanvItem* item);

#if 0
G_END_DECLS
#endif

#endif  /* LANV_ITEM_H */
