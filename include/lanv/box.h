/* This file is part of Lanv.
 * Copyright 2007-2014 David Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2025 Nedko Arnaudov
 *
 * Lanv is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Lanv is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Lanv.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LANV_BOX_H
#define LANV_BOX_H

#include <lanv/node.h>
#include <lanv/types.h>

#if 0
#include <glib-object.h>
#include <glib.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define LANV_TYPE_BOX            (lanv_box_get_type())
#define LANV_BOX(obj)            (GTK_CHECK_CAST((obj), LANV_TYPE_BOX, LanvBox))
#define LANV_BOX_CLASS(klass)    (GTK_CHECK_CLASS_CAST((klass), LANV_TYPE_BOX, LanvBoxClass))
#define LANV_IS_BOX(obj)         (GTK_CHECK_TYPE((obj), LANV_TYPE_BOX))
#define LANV_IS_BOX_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), LANV_TYPE_BOX))
#define LANV_BOX_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS((obj), LANV_TYPE_BOX, LanvBoxClass))

#endif

struct _LanvBoxClass;

typedef struct _LanvBoxClass   LanvBoxClass;
typedef struct _LanvBoxPrivate LanvBoxPrivate;

struct _LanvBox {
#if 0
	LanvNode        node;
	LanvBoxPrivate* impl;
#endif
};

#if 0
/**
 * LanvBoxClass:
 * @parent_class: Node superclass.
 * @set_width: Set the width of the box.
 * @set_height: Set the height of the box.
 */
struct _LanvBoxClass {
	LanvNodeClass parent_class;

	void (*set_width)(LanvBox* box,
	                  double   width);

	void (*set_height)(LanvBox* box,
	                   double   height);

	/* Reserved for future expansion */
	gpointer spare_vmethods[4];
};

GType  lanv_box_get_type(void) G_GNUC_CONST;

#endif

double lanv_box_get_x1(const LanvBox* box);
double lanv_box_get_y1(const LanvBox* box);
double lanv_box_get_x2(const LanvBox* box);
double lanv_box_get_y2(const LanvBox* box);
double lanv_box_get_width(const LanvBox* box);
void   lanv_box_set_width(LanvBox* box, double width);
double lanv_box_get_height(const LanvBox* box);
void   lanv_box_set_height(LanvBox* box, double height);
double lanv_box_get_border_width(const LanvBox* box);

/**
 * lanv_box_normalize:
 * @box: The box to normalize.
 *
 * Normalize the box coordinates such that x1 < x2 and y1 < y2.
 */
void lanv_box_normalize(LanvBox* box);

#if 0

G_END_DECLS

#endif

#endif  /* LANV_BOX_H */
