/* This file is part of Lanv.
 * Copyright 2007-2016 David Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2025 Nedko Arnaudov
 *
 * Lanv is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Lanv is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Lanv.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LANV_TYPES_H
#define LANV_TYPES_H

typedef struct _LanvCanvas LanvCanvas;
typedef struct _LanvEdge   LanvEdge;
typedef struct _LanvModule LanvModule;
typedef struct _LanvNode   LanvNode;
typedef struct _LanvPort   LanvPort;
typedef struct _LanvBox    LanvBox;

#endif  /* LANV_TYPES_H */
