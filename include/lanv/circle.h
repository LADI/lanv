/* This file is part of Lanv.
 * Copyright 2007-2014 David Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2025 Nedko Arnaudov
 *
 * Lanv is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Lanv is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Lanv.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LANV_CIRCLE_H
#define LANV_CIRCLE_H

#include <lanv/node.h>
#include <lanv/types.h>

#if 0

#include <glib-object.h>
#include <glib.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define LANV_TYPE_CIRCLE            (lanv_circle_get_type())
#define LANV_CIRCLE(obj)            (GTK_CHECK_CAST((obj), LANV_TYPE_CIRCLE, LanvCircle))
#define LANV_CIRCLE_CLASS(klass)    (GTK_CHECK_CLASS_CAST((klass), LANV_TYPE_CIRCLE, LanvCircleClass))
#define LANV_IS_CIRCLE(obj)         (GTK_CHECK_TYPE((obj), LANV_TYPE_CIRCLE))
#define LANV_IS_CIRCLE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), LANV_TYPE_CIRCLE))
#define LANV_CIRCLE_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS((obj), LANV_TYPE_CIRCLE, LanvCircleClass))

struct _LanvCircle;
struct _LanvCircleClass;

typedef struct _LanvCircle        LanvCircle;
typedef struct _LanvCircleClass   LanvCircleClass;
typedef struct _LanvCirclePrivate LanvCirclePrivate;

/**
 * LanvCircle:
 *
 * A circular #LanvNode.  A #LanvCircle is a leaf, that is, it does not contain
 * any child nodes (though, like any #LanvNode, it may have a label).
 */
struct _LanvCircle {
	LanvNode           node;
	LanvCirclePrivate* impl;
};

struct _LanvCircleClass {
	LanvNodeClass parent_class;

	/* Reserved for future expansion */
	gpointer spare_vmethods[4];
};

GType lanv_circle_get_type(void) G_GNUC_CONST;

LanvCircle*
lanv_circle_new(LanvCanvas* canvas,
                const char* first_prop_name, ...);

double
lanv_circle_get_radius(const LanvCircle* circle);

void
lanv_circle_set_radius(LanvCircle* circle, double radius);

double
lanv_circle_get_radius_ems(const LanvCircle* circle);

void
lanv_circle_set_radius_ems(LanvCircle* circle, double radius);

gboolean
lanv_circle_get_fit_label(const LanvCircle* circle);

void
lanv_circle_set_fit_label(LanvCircle* circle, gboolean fit_label);

G_END_DECLS

#endif

#endif  /* LANV_CIRCLE_H */
