/* This file is part of Lanv.
 * Copyright 2007-2015 David Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2025 Nedko Arnaudov
 *
 * Lanv is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Lanv is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Lanv.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LANV_CANVAS_H
#define LANV_CANVAS_H

#include <stdbool.h>

#include <lanv/item.h>
#include <lanv/types.h>

#include <cairo.h>
#if 0
#include <gdk/gdk.h>
#include <glib-object.h>
#include <glib.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define LANV_TYPE_CANVAS            (lanv_canvas_get_type())
#define LANV_CANVAS(obj)            (GTK_CHECK_CAST((obj), LANV_TYPE_CANVAS, LanvCanvas))
#define LANV_CANVAS_CLASS(klass)    (GTK_CHECK_CLASS_CAST((klass), LANV_TYPE_CANVAS, LanvCanvasClass))
#define LANV_IS_CANVAS(obj)         (GTK_CHECK_TYPE((obj), LANV_TYPE_CANVAS))
#define LANV_IS_CANVAS_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), LANV_TYPE_CANVAS))
#define LANV_CANVAS_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS((obj), LANV_TYPE_CANVAS, LanvCanvasClass))

struct _LanvCanvasClass;

typedef struct LanvCanvasImpl   LanvCanvasPrivate;
typedef struct _LanvCanvasClass LanvCanvasClass;

#endif

/**
 * LanvDirection:
 * @LANV_DIRECTION_DOWN: Signal flows from top to bottom.
 * @LANV_DIRECTION_RIGHT: Signal flows from left to right.
 *
 * Specifies the direction of signal flow on the canvas, which affects the
 * appearance of modules and how the canvas is auto-arranged.
 */
typedef enum {
	LANV_DIRECTION_DOWN,
	LANV_DIRECTION_RIGHT
} LanvDirection;

struct _LanvCanvas {
#if 0
	GtkLayout          layout;
	LanvCanvasPrivate* impl;
#endif
};

#if 0
struct _LanvCanvasClass {
	GtkLayoutClass parent_class;

	/* Reserved for future expansion */
	gpointer spare_vmethods[4];
};

GType lanv_canvas_get_type(void) G_GNUC_CONST;
#endif

/**
 * LanvEdgeFunc:
 * @edge: Canvas edge.
 * @data: User callback data.
 *
 * A node function that takes a user data argument (for callbacks).
 *
 * Note that in the Gtk world it is considered safe to cast a function to a
 * function with more arguments and call the resulting pointer, so functions
 * like lanv_edge_select can safely be used where a LanvEdgeFunc is expected.
 */
typedef void (*LanvEdgeFunc)(LanvEdge* edge, void* data);

/**
 * LanvNodeFunc:
 * @node: Canvas node.
 * @data: User callback data.
 *
 * A node function that takes a user data argument (for callbacks).
 *
 * Note that in the Gtk world it is considered safe to cast a function to a
 * function with more arguments and call the resulting pointer, so functions
 * like lanv_node_select can safely be used where a LanvNodeFunc is expected.
 */
typedef void (*LanvNodeFunc)(LanvNode* node, void* data);

/**
 * LanvPortOrderFunc:
 * @lhs: Left-hand port to compare.
 * @rhs: Right-hand port to compare.
 * @data: User callback data.
 *
 * A function to compare two ports.
 */
typedef int (*LanvPortOrderFunc)(const LanvPort* lhs, const LanvPort* rhs, void* data);

/**
 * lanv_canvas_new:
 *
 * Return value: A newly-created canvas.
 */
LanvCanvas*
lanv_canvas_new(double width, double height);

/**
 * lanv_canvas_set_wrapper:
 *
 * Set the opaque wrapper object for the canvas.
 */
void
lanv_canvas_set_wrapper(LanvCanvas* canvas, void* wrapper);

/**
 * lanv_canvas_get_wrapper:
 *
 * Return an opaque pointer to the wrapper for the canvas.
 */
void*
lanv_canvas_get_wrapper(LanvCanvas* canvas);

/**
 * lanv_canvas_clear:
 *
 * Remove all items from the canvas.
 */
void
lanv_canvas_clear(LanvCanvas* canvas);

/**
 * lanv_canvas_empty:
 *
 * Return value: True if there are no items on the canvas.
 */
bool
lanv_canvas_empty(const LanvCanvas* canvas);

/**
 * lanv_canvas_get_size:
 *
 * Gets the width and height of the canvas.
 */
void
lanv_canvas_get_size(LanvCanvas* canvas, double* width, double* height);

/**
 * lanv_canvas_resize:
 *
 * Resize the canvas to the given dimensions.
 */
void
lanv_canvas_resize(LanvCanvas* canvas, double width, double height);

/**
 * lanv_canvas_root:
 * @canvas: A canvas.
 *
 * Return value: (transfer none): The root group of the canvas.
 */
LanvItem*
lanv_canvas_root(LanvCanvas* canvas);

/**
 * lanv_canvas_set_scroll_region:
 * @canvas: A canvas.
 * @x1: Leftmost limit of the scrolling region.
 * @y1: Upper limit of the scrolling region.
 * @x2: Rightmost limit of the scrolling region.
 * @y2: Lower limit of the scrolling region.
 *
 * Sets the scrolling region of a canvas to the specified rectangle.  The
 * canvas will then be able to scroll only within this region.  The view of the
 * canvas is adjusted as appropriate to display as much of the new region as
 * possible.
 */
void
lanv_canvas_set_scroll_region(LanvCanvas* canvas,
                              double x1, double y1, double x2, double y2);

/**
 * lanv_canvas_get_scroll_region:
 * @canvas: A canvas.
 * @x1: Leftmost limit of the scrolling region (return value).
 * @y1: Upper limit of the scrolling region (return value).
 * @x2: Rightmost limit of the scrolling region (return value).
 * @y2: Lower limit of the scrolling region (return value).
 *
 * Queries the scrolling region of a canvas.
 */
void
lanv_canvas_get_scroll_region(LanvCanvas* canvas,
                              double* x1, double* y1, double* x2, double* y2);

/**
 * lanv_canvas_set_center_scroll_region:
 * @canvas: A canvas.
 * @center_scroll_region: Whether to center the scrolling region in the canvas
 * window when it is smaller than the canvas' allocation.
 *
 * When the scrolling region of the canvas is smaller than the canvas window,
 * e.g.  the allocation of the canvas, it can be either centered on the window
 * or simply made to be on the upper-left corner on the window.  This function
 * lets you configure this property.
 */
void
lanv_canvas_set_center_scroll_region(LanvCanvas* canvas,
                                     bool    center_scroll_region);

/**
 * lanv_canvas_get_center_scroll_region:
 * @canvas: A canvas.
 *
 * Returns whether the canvas is set to center the scrolling region in the
 * window if the former is smaller than the canvas' allocation.
 *
 * Returns: Whether the scroll region is being centered in the canvas window.
 */
bool
lanv_canvas_get_center_scroll_region(const LanvCanvas* canvas);

/**
 * lanv_canvas_scroll_to:
 * @canvas: A canvas.
 * @cx: Horizontal scrolling offset in canvas pixel units.
 * @cy: Vertical scrolling offset in canvas pixel units.
 *
 * Makes a canvas scroll to the specified offsets, given in canvas pixel units.
 * The canvas will adjust the view so that it is not outside the scrolling
 * region.  This function is typically not used, as it is better to hook
 * scrollbars to the canvas layout's scrolling adjustments.
 */
void
lanv_canvas_scroll_to(LanvCanvas* canvas, int cx, int cy);

/**
 * lanv_canvas_get_scroll_offsets:
 * @canvas: A canvas.
 * @cx: Horizontal scrolling offset (return value).
 * @cy: Vertical scrolling offset (return value).
 *
 * Queries the scrolling offsets of a canvas.  The values are returned in canvas
 * pixel units.
 */
void
lanv_canvas_get_scroll_offsets(const LanvCanvas* canvas, int* cx, int* cy);

/**
 * lanv_canvas_w2c_affine:
 * @canvas: A canvas.
 * @matrix: An affine transformation matrix (return value).
 *
 * Gets the affine transform that converts from world coordinates to canvas
 * pixel coordinates.
 */
void
lanv_canvas_w2c_affine(LanvCanvas* canvas, cairo_matrix_t* matrix);

/**
 * lanv_canvas_w2c:
 * @canvas: A canvas.
 * @wx: World X coordinate.
 * @wy: World Y coordinate.
 * @cx: X pixel coordinate (return value).
 * @cy: Y pixel coordinate (return value).
 *
 * Converts world coordinates into canvas pixel coordinates.
 */
void
lanv_canvas_w2c(LanvCanvas* canvas, double wx, double wy, int* cx, int* cy);

/**
 * lanv_canvas_w2c_d:
 * @canvas: A canvas.
 * @wx: World X coordinate.
 * @wy: World Y coordinate.
 * @cx: X pixel coordinate (return value).
 * @cy: Y pixel coordinate (return value).
 *
 * Converts world coordinates into canvas pixel coordinates.  This version
 * uses floating point coordinates for greater precision.
 */
void
lanv_canvas_w2c_d(LanvCanvas* canvas,
                  double      wx,
                  double      wy,
                  double*     cx,
                  double*     cy);

/**
 * lanv_canvas_c2w:
 * @canvas: A canvas.
 * @cx: Canvas pixel X coordinate.
 * @cy: Canvas pixel Y coordinate.
 * @wx: X world coordinate (return value).
 * @wy: Y world coordinate (return value).
 *
 * Converts canvas pixel coordinates to world coordinates.
 */
void
lanv_canvas_c2w(LanvCanvas* canvas, int cx, int cy, double* wx, double* wy);

/**
 * lanv_canvas_window_to_world:
 * @canvas: A canvas.
 * @winx: Window-relative X coordinate.
 * @winy: Window-relative Y coordinate.
 * @worldx: X world coordinate (return value).
 * @worldy: Y world coordinate (return value).
 *
 * Converts window-relative coordinates into world coordinates.  You can use
 * this when you need to convert mouse coordinates into world coordinates, for
 * example.
 */
void
lanv_canvas_window_to_world(LanvCanvas* canvas,
                            double      winx,
                            double      winy,
                            double*     worldx,
                            double*     worldy);

/**
 * lanv_canvas_world_to_window:
 * @canvas: A canvas.
 * @worldx: World X coordinate.
 * @worldy: World Y coordinate.
 * @winx: X window-relative coordinate.
 * @winy: Y window-relative coordinate.
 *
 * Converts world coordinates into window-relative coordinates.
 */
void
lanv_canvas_world_to_window(LanvCanvas* canvas,
                            double      worldx,
                            double      worldy,
                            double*     winx,
                            double*     winy);

/**
 * lanv_canvas_get_item_at:
 * @canvas: A canvas.
 * @x: X position in world coordinates.
 * @y: Y position in world coordinates.
 *
 * Looks for the item that is under the specified position, which must be
 * specified in world coordinates.
 *
 * Returns: (transfer none): The sought item, or NULL if no item is at the
 * specified coordinates.
 */
LanvItem*
lanv_canvas_get_item_at(LanvCanvas* canvas, double x, double y);

/**
 * lanv_canvas_get_edge:
 *
 * Get the edge between two nodes, or NULL if none exists.
 *
 * Return value: (transfer none): The root group of @canvas.
 */
LanvEdge*
lanv_canvas_get_edge(LanvCanvas* canvas,
                     LanvNode*   tail,
                     LanvNode*   head);

/**
 * lanv_canvas_remove_edge:
 *
 * Remove @edge from the canvas.
 */
void
lanv_canvas_remove_edge(LanvCanvas* canvas,
                        LanvEdge*   edge);

/**
 * lanv_canvas_remove_edge_between:
 *
 * Remove the edge from @tail to @head if one exists.
 */
void
lanv_canvas_remove_edge_between(LanvCanvas* canvas,
                                LanvNode*   tail,
                                LanvNode*   head);

/**
 * lanv_canvas_get_direction:
 *
 * Return the direction of signal flow.
 */
LanvDirection
lanv_canvas_get_direction(LanvCanvas* canvas);

/**
 * lanv_canvas_set_direction:
 *
 * Set the direction of signal flow.
 */
void
lanv_canvas_set_direction(LanvCanvas* canvas, LanvDirection dir);

/**
 * lanv_canvas_arrange:
 *
 * Automatically arrange the canvas contents.
 */
void
lanv_canvas_arrange(LanvCanvas* canvas);

/**
 * lanv_canvas_export_image:
 *
 * Draw the canvas to an image file.  The file type is determined by extension,
 * currently supported: pdf, ps, svg, dot.
 *
 * Returns: 0 on success.
 */
int
lanv_canvas_export_image(LanvCanvas* canvas,
                         const char* filename,
                         bool    draw_background);

/**
 * lanv_canvas_export_dot:
 *
 * Write a Graphviz DOT description of the canvas to a file.
 */
void
lanv_canvas_export_dot(LanvCanvas* canvas, const char* filename);

/**
 * lanv_canvas_supports_sprung_layout:
 *
 * Returns: true iff lanv is compiled with sprung layout support.
 */
bool
lanv_canvas_supports_sprung_layout(const LanvCanvas* canvas);

/**
 * lanv_canvas_set_sprung_layout:
 *
 * Enable or disable "live" force-directed canvas layout.
 *
 * Returns: true iff sprung layout was enabled.
 */
bool
lanv_canvas_set_sprung_layout(LanvCanvas* canvas, bool sprung_layout);

/**
 * lanv_canvas_get_locked:
 *
 * Return true iff the canvas is locked and nodes may not move.
 */
bool
lanv_canvas_get_locked(const LanvCanvas* canvas);

/**
 * lanv_canvas_for_each_node:
 * @canvas: The canvas.
 * @f: (scope call): A function to call on every node on @canvas.
 * @data: Data to pass to @f.
 */
void
lanv_canvas_for_each_node(LanvCanvas*  canvas,
                          LanvNodeFunc f,
                          void*        data);

/**
 * lanv_canvas_for_each_selected_node:
 * @canvas: The canvas.
 * @f: (scope call): A function to call on every selected node on @canvas.
 * @data: Data to pass to @f.
 */
void
lanv_canvas_for_each_selected_node(LanvCanvas*  canvas,
                                   LanvNodeFunc f,
                                   void*        data);

/**
 * lanv_canvas_for_each_edge:
 * @canvas: The canvas.
 * @f: (scope call): A function to call on every edge on @canvas.
 * @data: Data to pass to @f.
 */
void
lanv_canvas_for_each_edge(LanvCanvas*  canvas,
                          LanvEdgeFunc f,
                          void*        data);

/**
 * lanv_canvas_for_each_edge_from:
 * @canvas: The canvas.
 * @tail: The tail to enumerate every edge for.
 * @f: (scope call): A function to call on every edge leaving @tail.
 */
void
lanv_canvas_for_each_edge_from(LanvCanvas*     canvas,
                               const LanvNode* tail,
                               LanvEdgeFunc    f,
                               void*           data);

/**
 * lanv_canvas_for_each_edge_to:
 * @canvas: The canvas.
 * @head: The head to enumerate every edge for.
 * @f: (scope call): A function to call on every edge entering @head.
 */
void
lanv_canvas_for_each_edge_to(LanvCanvas*     canvas,
                             const LanvNode* head,
                             LanvEdgeFunc    f,
                             void*           data);

/**
 * lanv_canvas_for_each_edge_on:
 * @canvas: The canvas.
 * @node: The node to enumerate every edge for.
 * @f: (scope call): A function to call on every edge attached to @node.
 */
void
lanv_canvas_for_each_edge_on(LanvCanvas*     canvas,
                             const LanvNode* node,
                             LanvEdgeFunc    f,
                             void*           data);

/**
 * lanv_canvas_for_each_selected_edge:
 * @canvas: The canvas.
 * @f: (scope call): A function to call on every edge attached to @node.
 * @data: Data to pass to @f.
 */
void
lanv_canvas_for_each_selected_edge(LanvCanvas*  canvas,
                                   LanvEdgeFunc f,
                                   void*        data);

/**
 * lanv_canvas_select_all:
 *
 * Select all items on the canvas.
 */
void
lanv_canvas_select_all(LanvCanvas* canvas);

/**
 * lanv_canvas_clear_selection:
 *
 * Deselect any selected items on the canvas.
 */
void
lanv_canvas_clear_selection(LanvCanvas* canvas);

/**
 * lanv_canvas_get_zoom:
 *
 * Return the current zoom factor (pixels per unit).
 */
double
lanv_canvas_get_zoom(const LanvCanvas* canvas);

/**
 * lanv_canvas_set_zoom:
 * @canvas: A canvas.
 * @zoom: The number of pixels that correspond to one canvas unit.
 *
 * The anchor point for zooming, i.e. the point that stays fixed and all others
 * zoom inwards or outwards from it, depends on whether the canvas is set to
 * center the scrolling region or not.  You can control this using the
 * lanv_canvas_set_center_scroll_region() function.  If the canvas is set to
 * center the scroll region, then the center of the canvas window is used as
 * the anchor point for zooming.  Otherwise, the upper-left corner of the
 * canvas window is used as the anchor point.
 */
void
lanv_canvas_set_zoom(LanvCanvas* canvas, double zoom);

/**
 * lanv_canvas_zoom_full:
 *
 * Zoom so all canvas contents are visible.
 */
void
lanv_canvas_zoom_full(LanvCanvas* canvas);

/**
 * lanv_canvas_get_default_font_size:
 *
 * Get the default font size in points.
 */
double
lanv_canvas_get_default_font_size(const LanvCanvas* canvas);

/**
 * lanv_canvas_get_font_size:
 *
 * Get the current font size in points.
 */
double
lanv_canvas_get_font_size(const LanvCanvas* canvas);

/**
 * lanv_canvas_set_font_size:
 *
 * Set the current font size in points.
 */
void
lanv_canvas_set_font_size(LanvCanvas* canvas, double points);

#if 0
/**
 * lanv_canvas_get_move_cursor:
 *
 * Return the cursor to use while dragging canvas objects.
 */
GdkCursor*
lanv_canvas_get_move_cursor(const LanvCanvas* canvas);
#endif

/**
 * lanv_canvas_move_contents_to:
 *
 * Shift all canvas contents so the top-left object is at (x, y).
 */
void
lanv_canvas_move_contents_to(LanvCanvas* canvas, double x, double y);

/**
 * lanv_canvas_set_port_order:
 * @canvas: The canvas to set the default port order on.
 * @port_cmp: (scope call): Port comparison function.
 * @data: Data to be passed to order.
 *
 * Set a comparator function to use as the default order for ports on modules.
 * If left unset, ports are shown in the order they are added.
 */
void
lanv_canvas_set_port_order(LanvCanvas*       canvas,
                           LanvPortOrderFunc port_cmp,
                           void*             data);


#if 0
G_END_DECLS
#endif

#endif  /* LANV_CANVAS_H */
