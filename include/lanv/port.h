/* This file is part of Lanv.
 * Copyright 2007-2014 David Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2025 Nedko Arnaudov
 *
 * Lanv is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Lanv is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Lanv.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LANV_PORT_H
#define LANV_PORT_H

#include <lanv/box.h>
#include <lanv/types.h>

#if 0

#include <glib-object.h>
#include <glib.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define LANV_TYPE_PORT            (lanv_port_get_type())
#define LANV_PORT(obj)            (GTK_CHECK_CAST((obj), LANV_TYPE_PORT, LanvPort))
#define LANV_PORT_CLASS(klass)    (GTK_CHECK_CLASS_CAST((klass), LANV_TYPE_PORT, LanvPortClass))
#define LANV_IS_PORT(obj)         (GTK_CHECK_TYPE((obj), LANV_TYPE_PORT))
#define LANV_IS_PORT_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), LANV_TYPE_PORT))
#define LANV_PORT_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS((obj), LANV_TYPE_PORT, LanvPortClass))

#endif

struct _LanvPortClass;

typedef struct _LanvPortClass   LanvPortClass;
typedef struct _LanvPortPrivate LanvPortPrivate;

struct _LanvPort {
#if 0
	LanvBox          box;
	LanvPortPrivate* impl;
#endif
};

#if 0

struct _LanvPortClass {
	LanvBoxClass parent_class;

	/* Reserved for future expansion */
	gpointer spare_vmethods[4];
};

GType lanv_port_get_type(void) G_GNUC_CONST;

LanvPort*
lanv_port_new(LanvModule* module,
              gboolean    is_input,
              const char* first_prop_name, ...);

void
lanv_port_set_value_label(LanvPort*   port,
                          const char* str);

void
lanv_port_show_control(LanvPort* port);

void
lanv_port_hide_control(LanvPort* port);

void
lanv_port_set_control_is_toggle(LanvPort* port,
                                gboolean  is_toggle);

void
lanv_port_set_control_is_integer(LanvPort* port,
                                 gboolean  is_integer);

void
lanv_port_set_control_value(LanvPort* port,
                            float     value);

void
lanv_port_set_control_min(LanvPort* port,
                          float     min);

void
lanv_port_set_control_max(LanvPort* port,
                          float     max);

double
lanv_port_get_natural_width(const LanvPort* port);

/**
 * lanv_port_get_module:
 * @port: The port.
 *
 * Return value: (transfer none): The module @port is on.
 */
LanvModule* lanv_port_get_module(const LanvPort* port);

float    lanv_port_get_control_value(const LanvPort* port);
float    lanv_port_get_control_min(const LanvPort* port);
float    lanv_port_get_control_max(const LanvPort* port);
gboolean lanv_port_is_input(const LanvPort* port);
gboolean lanv_port_is_output(const LanvPort* port);

G_END_DECLS

#endif

#endif  /* LANV_PORT_H */
