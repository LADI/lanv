/* This file is part of Lanv.
 * Copyright 2007-2014 David Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2025 Nedko Arnaudov
 *
 * Lanv is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Lanv is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Lanv.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LANV_GROUP_H
#define LANV_GROUP_H

#include <lanv/item.h>

#if 0
#include <glib-object.h>
#include <glib.h>

G_BEGIN_DECLS

/* Based on GnomeCanvasGroup, by Federico Mena <federico@nuclecu.unam.mx>
 * and Raph Levien <raph@gimp.org>
 * Copyright 1997-2000 Free Software Foundation
 */

#define LANV_TYPE_GROUP            (lanv_group_get_type())
#define LANV_GROUP(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj), LANV_TYPE_GROUP, LanvGroup))
#define LANV_GROUP_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass), LANV_TYPE_GROUP, LanvGroupClass))
#define LANV_IS_GROUP(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj), LANV_TYPE_GROUP))
#define LANV_IS_GROUP_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), LANV_TYPE_GROUP))
#define LANV_GROUP_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), LANV_TYPE_GROUP, LanvGroupClass))

#endif

struct _LanvGroup;
struct _LanvGroupClass;

typedef struct _LanvGroup        LanvGroup;
typedef struct _LanvGroupPrivate LanvGroupPrivate;
typedef struct _LanvGroupClass   LanvGroupClass;

struct _LanvGroup {
#if 0
	LanvItem          item;
	LanvGroupPrivate* impl;
#endif
};

#if 0
struct _LanvGroupClass {
	LanvItemClass parent_class;

	/* Reserved for future expansion */
	gpointer spare_vmethods[4];
};

GType lanv_group_get_type(void) G_GNUC_CONST;

G_END_DECLS
#endif

#endif  /* LANV_GROUP_H */
