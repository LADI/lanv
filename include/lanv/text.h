/* This file is part of Lanv.
 * Copyright 2007-2014 David Robillard <http://drobilla.net>
 * SPDX-FileCopyrightText: Copyright (C) 2025 Nedko Arnaudov
 *
 * Lanv is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or any later version.
 *
 * Lanv is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Lanv.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LANV_TEXT_H
#define LANV_TEXT_H

#include <lanv/item.h>

#if 0
#include <glib-object.h>
#include <glib.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define LANV_TYPE_TEXT            (lanv_text_get_type())
#define LANV_TEXT(obj)            (GTK_CHECK_CAST((obj), LANV_TYPE_TEXT, LanvText))
#define LANV_TEXT_CLASS(klass)    (GTK_CHECK_CLASS_CAST((klass), LANV_TYPE_TEXT, LanvTextClass))
#define LANV_IS_TEXT(obj)         (GTK_CHECK_TYPE((obj), LANV_TYPE_TEXT))
#define LANV_IS_TEXT_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), LANV_TYPE_TEXT))
#define LANV_TEXT_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS((obj), LANV_TYPE_TEXT, LanvTextClass))
#endif

struct _LanvText;
struct _LanvTextClass;

typedef struct _LanvText        LanvText;
typedef struct _LanvTextClass   LanvTextClass;
typedef struct _LanvTextPrivate LanvTextPrivate;

struct _LanvText {
#if 0
	LanvItem         item;
	LanvTextPrivate* impl;
#endif
};

#if 0
struct _LanvTextClass {
	LanvItemClass parent_class;

	/* Reserved for future expansion */
	gpointer spare_vmethodsx[4];
};

GType lanv_text_get_type(void) G_GNUC_CONST;
#endif

void lanv_text_layout(LanvText* text);

#if 0
G_END_DECLS
#endif

#endif  /* LANV_TEXT_H */
